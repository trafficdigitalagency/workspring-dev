In addition to this document, the WordPress documentation site (http://codex.wordpress.org/) and community support forums (http://wordpress.org/support/) are other good sources of information about the software used to build the James Dyson Foundation website.

# The Wordpress Dashboard

Accessing the WordPress Dashboard for your site is as simple as going to http://workspring.com/wp-admin/ and entering your account’s username and password. There are no links to the Dashboard anywhere on the front end of the site, so please bookmark this URL.

You can customize which boxes are displayed on this page (and on any other page in the Dashboard) by clicking the "Screen Options" tab in the top right corner of the screen, which expands a menu allowing you to toggle on and off the available boxes for that admin page. If you can't find a box on a page that used to be there, be sure to check the Screen Options tab to see if it has somehow been turned off. You can also customize where the boxes on the screen are located by clicking the top stripe of a box and dragging it into a different position.

[screen_options]

Please note that toggling boxes on and off and moving boxes around on the screen only affects your account. Other users have their own custom Screen Options settings and any changes you make will not be carried over to their accounts.

# General Wordpress Functionality

## Pages, Posts, and other items

Pages, posts, other resources can all be added and edited in a similar fashion.

To view existing items, click on the item type in the sidebar. A listing of all items will appear.

To add an item to one of these groups, click on the item type in the sidebar, followed by the "Add New" button in the upper left. A shortcut also exists in the sidebar and top bar for most item types.

## Title and Slug

The title for the item can be entered in the large field. This title will be used in the item description that appears in your browser’s title bar and be used to create the item's URL.

Once you have entered a title for your new item, you’ll notice a line appears underneath the title field showing the URL for your new item. The section of the URL created from your title is called the item “slug”. Whenever you create a new item, the slug will automatically be created for you based on your item title. If your item title was "Hello world!", the item’s automatically-generated slug would be "hello-world". If you edit a item’s title in the future, the slug will not change and you should manually edit it to match the new item title.

Only alphabet, numeric, and hyphen characters are allowed in an item's slug. Slugs will automatically adjust themselves to be unique within an item type. It is very important to have item titles (and slugs) that accurately describe the content on your item to help people find your site in Google searches.

## Content

The item's content can be entered in the large field below the title field. This field has handy controls that allow you to add formatting to the text. More advanced options are available by clicking the last button in the top row of buttons. For more detailed help with the rich editor, click the question mark button in the second row of buttons in the advanced section.

## Media

To add images and other media into your item, place the text cursor at the point where you would like the media to be inserted and click the "Add Media" button. This will pop up an interface for adding media from the Wordpress media library.

[media_library]

Media can be inserted either by selecting existing media items or uploading new items.

Existing items can be selected by clicking "Media Library" and clicking on the desired item. The item's information can be edited as well as the specifics of the way in which it will be inserted including the alignment, whether the item will link to a location, and the size of the media. The item is inserted by clicking the "Insert into post" button.

New items can be uploaded by selecting "Upload Files". Files can be dragged and dropped into the window pane or selected by clicking the "Select Files" button. The files will be uploaded and added to the site's media library.

Many item types can be set to have a featured image. An item's featured image is set by clicking "Set featured image" or the previously set image.

## Image Sizes

Front page slider - 1600 x 465
Front page news slider - 450 x 490
News image - => 284 x 210
Post detail - => 634 x 634
Location video - 980 x 475
Location header - 1600 x 250
Offering image - 234 x 234
Header image - => 1600 x 250
Person - width'  => 293 x 442
Concierge - width'  => 72 x 72
Choose location - 1600 x 410
Mobile header - 320 x 212
Mobile front page slider - 320 x 402
Mobile front page news slider - 320 x 350
Mobile location image - 264 x 191
Mobile large news image - 264 x 350
Large news image - 633 x 468

## Featured Image

The featured image is the image that appears in listings and at the top of the post’s detail page. It can be set by clicking “Set featured image” or the currently set featured image.

## Categories, Tags, and Taxonomies

Most items in Wordpress can be associated with a number of categories, tags. This is done through panels on the right side of the editor page. Categories are added by either selecting existing categories from the checklist or clicking "Add New Category" and typing in the name of the new category. Tags are added by either entering the tag in the input field and clicking add or selecting existing tags by clicking "Choose from the most used tags" and selecting from the tag cloud.

Don't forget to save your changes!

## Excerpt

The excerpt controls the text that appears for a post in listing views. If the excerpt is not set, an excerpt will be automatically generated from the post content. The excerpt cannot include HTML tags or styling (bold or italic text, links, etc).

## Ordering

Posts often appear in a listing view. These listing views can be sorted either by date, title, or an assigned order. To set the assigned order of a post, enter a value in the “Order” field in the “Attributes” box. A higher number will give the post higher priority.

[page_attributes]

# Post Type Specifics

## FAQ

The admin for the FAQ page features a table for categories of questions. In each row is another table for questions and answers.

## Partnerships

The admin screen for the partnerships page will allow you to select which pages or posts to feature as featured partnerships.

## People

The admin screen for people also has an input for the person's position.

Their name should be input as the title.

Featured posts pull from the person's related posts.

## Why Workspring

The admin screen for the partnerships page will allow you to select which pages or posts to feature as featured.

## Front Page

### Sliders

The sliders for the front page allow input of a text heading, text subheading, button label, button URL, an image, and a text caption. Links can also be specified as videos which will cause them to launch in a modal.

## Location

### General

The admin page for the general location tab features inputs for the featured quote, hours, contact, phone, and email.

The address for the location is specified by searching for the address via a custom Google Maps interface.

The tour URL should be specified as the direct link to the tour in Google maps.

The video URL should be specified as the direct link to the Youtube video.

The featured event is set by selecting the related post.

### Social

Multiple social links can be specified by adding a row and entering the type of link and the URL.

### Offerings

Location offerings tabs are specified by selecting the related offering.

#### Slider

Location offerings can include images with captions in a slider.

#### Space details

Location offerings also can include a table of details. Each row has an input for a space name, capacity, dimensions, tools & technology, floorplan URL, and tour URL.

### Local guide

The location local guide has a table for categories. In each row is an additional table for URLs and labels.

### Find Us

The "find" tab's map pulls from the address set on the general tab.

Driving directions are input via the Wordpress content editor.

### Pricing

The admin for the pricing and promotions page allows you to select related posts or page to feature.

# Navigation

You can access the Menus page by hovering over the “Appearance” option in the left sidebar menu and selecting “Menus” from the popout menu. From this page, you can add, remove, or (drag-and-drop) reorder the links in the site header.

[menus_in_menu]

There are two ways to add a new link to a menu. The Custom Link Box allows you to add a link to any URL to your menu. The Pages Box lists all the WordPress pages on the site, allowing you to easily add links to the menu without entering the page’s URL. Whenever possible, you should use the Pages Box to add links to your menu.

[navigation]

To make a link open in a new window, make sure the "Link Target" field is enabled by opening the "Screen Options" tab at the top right of the screen. Make sure the checkbox for the field is checked. In the editor for the menu item, check the box next to "Open link in a new window/tab".

Once you're finished adding, removing, and/or reordering the links in your menu, hit the "Save Menu" button at the bottom and your changes will be made live on the website.

The primary navigation menu makes use of CSS classes to set the color for the menu item. The four classes needed are green, red, blue, and purple. These should be entered in the “CSS classes” field for the menu item.

# Forms

Forms are managed through the Gravity Forms plugin. Documentation and detailed user guides are available on the Gravity Forms website (http://www.gravityhelp.com/documentation/page/Gravity_Forms_Documentation).

Certain dropdown fields automatically populate with data from the site.

## Populate locations

"populate-locations" class and "book-location" parameter are required

## Populate services

"populate-services" class and "book-service" parameter are required

## Populate guests

"populate-guests" class and "book-guest" parameter are required

# General Options

## Booking Modal

The booking modal text is entered on the general options page. An image for the concierge can be specified as well.

# Users

Each person who needs access to the WordPress Dashboard should have their own user account. This makes it easier to track who created or edited content, as well as giving you the ability to deny access to the Dashboard to a particular person by deleting their user account.

WordPress has five built-in roles for user accounts. From least to most
important, they are:

    •   Subscribers can only access and edit their own user profile.
    •   Contributors can create and edit their own posts and pages, but cannot publish them. They cannot edit posts or pages created by other users.
    •   Authors can create, edit, and publish their own posts and pages. They cannot edit posts or pages created by other users.
    •   Editors can create, edit, and publish their own posts and pages, as well as edit and republish posts and pages created by other users.
    •   Administrators can create, edit, and publish any post or page and have access to the entire Dashboard with no restrictions.

Depending on who you’re creating a new account for, you should consider restricting their ability to publish to the live website or edit
content created by other users unless absolutely necessary.

# User Registration

Users are automatically created and logged in upon completion of the registration form. They are created as subscribers and can only edit their projects from the front end of the site. Administrators will be notified of the user registration.
