// Add all initializing JS inside document.ready

var iframe;
var player;

$(document).ready(function(){
  $('.video-modals .video-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    touchMove: true,
    touchThreshold: 5
  });

  //$(".video-slide").fitVids();

  $(".tda-trig").on("change", function() {
    if ($(this).is(":checked")) {
      var x = $(this).attr('id');

      $("body").addClass("tda-modal-open");

      $('.video-modals .video-slider').slickGoTo( parseInt(x) );

      //var videoID = $('.slick-track').find('.slick-active').find('iframe').attr('id');
      //console.log(videoID);
      //iframe = $('#' + videoID + '')[0];
      //player = $f(iframe);

      //player.api("play");

    } else {
      $("body").removeClass("tda-modal-open");
      player.api("pause");
    }
  });

  $(".tda-modal-fade, .tda-modal-close").on("click", function() {
    $(".tda-modal-state:checked").prop("checked", false).change();
    player.api("pause");

  });

  //Slider Next Arrow Handlers
  $('.slick-next, .slick-prev').on('click', function(){
    player.api("pause");

  });

  $('.video-modals .video-slider').on('afterChange', function(){
    var videoID = $('.slick-track').find('.slick-active').find('iframe').attr('id');
    console.log( videoID );
    iframe = $('#' + videoID + '')[0];
    player = $f(iframe);
    player.api("play");
  });

  $(".tda-modal-inner").on("click", function(e) {
    e.stopPropagation();
  });

  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

});
