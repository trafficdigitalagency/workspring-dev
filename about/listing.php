<?php
if (get_field("articles")) {
    $relatedQuery = new WP_Query(
        array(
            "post_type"           => "any",
            "post__in"            => get_field("articles"),
            "ignore_sticky_posts" => true,
            "orderby"             => "post__in",
            "posts_per_page"      => -1,
        )
    );

    if ($relatedQuery->have_posts()) {
        ?>
        <div class="row about-listing">
        <?php

        while ($relatedQuery->have_posts()) {
            $relatedQuery->the_post();
            ?>
            <div class="col-md-4 listing-item <?php echo SOP_nthChildClass($relatedQuery->current_post); ?>">
                <?php get_template_part("partials/listing-item"); ?>
            </div>
            <?php
        } // endwhile posts
        ?>
        </div>
        <?php
    } // endif posts
}
