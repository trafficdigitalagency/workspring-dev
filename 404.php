<?php

get_template_part("partials/head");

?>
<div class="container">
    <div class="content-container">
        <h1 class="sr-only">404</h1>

        <h2>Oops! We couldn't find the page you were looking for.</h2>

        <p>To continue exploring the site, please return to our <a href="<?php echo site_url(); ?>">home page</a> or use the menu at the top of this page. </p>

        <p>If you experience further problems, please <a href="<?php the_field("contact_page", "options"); ?>">contact us</a>.</p>
    </div>
</div>
<?php

get_template_part("partials/foot");
