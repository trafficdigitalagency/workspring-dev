<?php
SOP_loopOverLocations(function () {
    while (have_posts()) {
        the_post();
        ?>
        <li data-value="<?php the_id(); ?>"
            class="dropdown-item">
            <a href="javascript:void(0)">
                <?php the_title(); ?>
            </a>
        </li>
        <?php
    } // endwhile locations
});
