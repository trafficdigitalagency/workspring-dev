<div class="offering">
    <div class="offering-inner">
        <div class="offering-overview offering-panel">
            <h2 class="large-heading">
                <a href="javascript:void(0)">
                    <?php the_title(); ?>
                </a>
            </h2>
            <div class="offering-text">

                <p class="offering-starts-at"><?php the_field("starting_cost"); ?></p>

                <p class="offering-overview-text"><?php the_field("overview"); ?></p>
            </div>

            <div class="offering-overview-bottom">
                <a href="javascript:void(0)" class="offerings-image-outer">
                    <?php
                    the_post_thumbnail(
                        'offering-image',
                        array("class" => "img-circle offerings-image center-block")
                    );
                    ?>
                </a>

                <div class="offering-info-button-outer">
                    <a href="javascript:void(0)" class="btn btn-primary offering-info-button">
                        <?php _e('MORE INFO', 'workspring'); ?>
                    </a>
                </div>
            </div>

        </div>

        <div class="offering-amenities offering-panel">
            <div class="offering-amenities-inner">
                <h3 class="medium-heading">
                    <?php _e('All-inclusive Amenities', 'workspring'); ?>
                </h3>

                <div class="offering-amentities-data">
                    <?php get_template_part("offerings/amenities"); ?>
                </div>

                <?php the_field("amenities_callout"); ?>

                <?php
                if (have_rows("ctas")) {
                    while (have_rows("ctas")) {
                        the_row();
                        ?>
                        <a href="<?php echo SOP_bookCTAUrl(); ?>" class="btn btn-primary hidden-lg hidden-md hidden-sm">
                            <?php the_sub_field("label"); ?>
                        </a>
                        <?php
                    } // endwhile
                } // endif ?>
            </div>
        </div>

        <div class="offering-pricing offering-panel">
            <a class="close-offering" href="javascript:void(0)">
                <div class="sr-only"><?php _e('Close', 'workspring'); ?></div>
            </a>

            <h3 class="medium-heading">
                <?php _e('Price Calculator', 'workspring'); ?>
            </h3>

            <div class="offering-pricing-calculator" data-id="<?php the_id(); ?>"></div>
        </div>
    </div>
</div>
