<?php
if (have_rows("amenities")) {
    $index = 0;
    while (have_rows("amenities")) {
        the_row();
        ?>
        <div class="offering-amentities-row <?php echo SOP_oddClass($index); ?>">
            <p><?php the_sub_field("info"); ?></p>
        </div>
        <?php
        $index++;
    } // endwhile amenities
} // endif amenities
