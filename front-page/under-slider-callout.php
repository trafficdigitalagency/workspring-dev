<?php if(get_field("under_slider_callout")) { ?>
    <div class="under_slider_callout">
        <div class="container">
            <?php the_field("under_slider_callout"); ?>
            <?php if(get_field("under_slider_callout_cta")) { ?>
                <a  <?php if(get_field("under_slider_callout_link_type") == "internal") { ?>
                        href="<?php the_field("under_slider_callout_link") ?>"
                    <?php } else { ?>
                        href="<?php the_field("under_slider_callout_custom_link") ?>"
                    <?php } ?>
                    class="btn btn-primary">
                    <?php the_field("under_slider_callout_cta"); ?>
                </a>
            <?php } ?>
        </div>
    </div>

    <style>
    .under_slider_callout {
        padding: 20px 0;
        border-bottom:1px solid #dedede;

        font-family: 'Archer SSm A', 'Archer SSm B', Georgia, 'Times New Roman', Times, serif;
        text-align: center;
        font-size: 1.5em;
    }
    .under_slider_callout a {
        margin: 0 1.5em;
        padding: 0.2em 0.8em;

        font-size: 1em;
        font-family: 'Archer SSm A', 'Archer SSm B', Georgia, 'Times New Roman', Times, serif;
        letter-spacing: 0;

        vertical-align: baseline;
    }
    @media (max-width: 767px) {
        .under_slider_callout {
            display: none;
        }
    }
    </style>

<?php } ?>
