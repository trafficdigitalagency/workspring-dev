<div class="twitter-feed flex-3">
    <h2>@Workspring</h2>
    <div class="slides twitter-slider slick-white" slick="twitter">
        <?php
        $tweets = stf_get_tweets(
            array(
                'order' => 'DESC',
                'orderby' => 'date'
            )
        );
        if (! empty($tweets)) {
            foreach ($tweets as $tweet) {
                ?>
                <div class="slide">
                    <div class="slide-inner">
                        <div class="tweet-content">
                            <p><?php echo $tweet->content; ?></p>
                        </div>
                        <div class="tweet-date">
                            <?php echo $tweet->time_str; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
