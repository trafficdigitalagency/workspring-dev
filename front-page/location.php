<?php
SOP_loopOverLocations(function () {
    ?>
    <div class="choose-location flex-1" style="background-image: url(<?php echo SOP_getThumbnailSrc("choose-location"); ?>);">
        <div class="container">
            <div class="text">
                <div class="row">
                    <div class="col-md-6">
                        <h2><?php _e('Choose your location', 'workspring'); ?></h2>
                    </div>
                    <div class="col-md-6">
                        <?php
                        if (!get_field('location_search', 'option')) {
                            ?>
                        <div class="dropdown">
                            <a href="javascript:void(0)"
                                class="btn btn-primary" data-toggle="dropdown">
                                Select your location
                            </a>
                            <ul class="dropdown-menu">
                                <?php
                                while (have_posts()) {
                                    the_post();
                                    ?>
                                    <li class="dropdown-item">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </li>
                                    <?php
                                } // endwhile
                                ?>
                            </ul>
                        </div>
                            <?php
                        } else {
                            ?>
                        <form action="<?php echo site_url('/location-search'); ?>" class="gform_wrapper">
                            <input type="text" name="address" placeholder="Your Address">
                        </form>
                            <?php
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
});
