<?php
if (have_rows("news_slider")) {
    ?>
    <div class="news news-outer flex-3">
        <div class="container">
            <h2 class="sr-only"><?php _e('News', 'workspring'); ?></h2>
            <div class="slides news-slider" slick="home-news">
    <?php
    while (have_rows("news_slider")) {
        the_row(); ?>
            <div class="slide">
                <div class="news-slide-inner"
                    style="background-image: url(<?php echo SOP_slideBackground("front-page-news-slider"); ?>)">
                    <div class="slide-text">
                        <div class="slide-text-inner">
                            <h3 class="large-heading">
                                <?php the_sub_field("heading"); ?>
                            </h3>
                            <p><?php the_sub_field("subheading"); ?></p>
                            <a href="<?php the_sub_field("url"); ?>"
                                class="btn btn-primary <?php echo SOP_videoClass(); ?>"
                                <?php echo SOP_slideModalAttr(); ?>>
                                <?php the_sub_field("cta"); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    <?php
    } // endwhile
    ?>
            </div>
        </div>
    </div>
    <?php
} // endif
