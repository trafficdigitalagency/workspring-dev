<?php
if (have_rows("top_slider")) {
    ?>
    <div class="slides slides-home slick-white flex-2" slick="home">
    <?php
    while (have_rows("top_slider")) {
        the_row();
        ?>
        <div class="slide" style="background-image: url(<?php echo SOP_slideBackground(); ?>)">
            <div class="slide-text">
                <h2>
                    <?php the_sub_field("heading"); ?>
                </h2>
                <p><?php the_sub_field("subheading"); ?></p>
                <a href="<?php the_sub_field("url"); ?>"
                    class="btn btn-primary <?php echo SOP_videoClass(); ?>"
                    <?php echo SOP_slideModalAttr(); ?>>
                    <?php the_sub_field("cta"); ?>
                </a>
            </div>

            <div class="slide-caption">
                <?php the_sub_field("caption"); ?>
            </div>
        </div>
        <?php
    } // endwhile
    ?>
    </div>
    <?php
} // endif
?>
