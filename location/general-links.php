<?php
/**
 * Location General Links
 */

if (get_field("links")) {
    ?>
    <ul class="location-general-links">
    <?php
    while (has_sub_field("links")) {
        ?>
        <li class="location-general-link">
            <a href="<?php echo SOP_getLinkURL(); ?>"
                target="<?php the_sub_field("target"); ?>">
                <?php the_sub_field("label"); ?>
            </a>
        </li>
        <?php
    } // endwhile links
    ?>
    </ul>
    <?php
} // endif linkst
