<div class="row <?php echo SOP_oddClass(apply_filters("someoddpilot\LoopTemplate\loopIndex", 0)); ?>">
    <div class="offering-table-col col-name">
        <p><?php the_sub_field("space_name"); ?></p>
        <?php
        if (get_sub_field("virtual_tour_url")) {
            ?>
            <p>
                <a href="<?php the_sub_field("virtual_tour_url"); ?>"
                    title="<?php _e('Take Virtual Tour', 'workspring'); ?>"
                    target="_blank">
                    <?php _e('Take Virtual Tour', 'workspring'); ?>
                </a>
            </p>
            <?php
        } // endif virtual tour ?>
    </div>
    <div class="offering-table-col col-capacity">
        <div class="hidden-sm hidden-md hidden-lg"><?php _e('Capacity', 'workspring'); ?></div>
        <p><?php the_sub_field("capacity"); ?></p>
    </div>
    <div class="offering-table-col col-dimensions">
        <div class="hidden-sm hidden-md hidden-lg"><?php _e('Dimensions', 'workspring'); ?></div>
        <p><?php the_sub_field("dimensions"); ?></p>
    </div>
    <div class="offering-table-col col-tools">
        <div class="hidden-sm hidden-md hidden-lg"><?php _e('Tools &amp; Technology', 'workspring'); ?></div>
        <p><?php the_sub_field("tools_and_technology"); ?></p>
    </div>
    <div class="offering-table-col col-desktop-floorplan hidden-xs">
        <?php
        if (get_sub_field("floorplan")) {
            ?>
            <a class="icon-download" href="<?php the_sub_field("floorplan"); ?>"
                download
                title="<?php _e('Floorplan', 'workspring'); ?>" target="_blank">
                <span class="sr-only"><?php _e('Floorplan', 'workspring'); ?></span>
            </a>
            <?php
        } ?>
    </div>
    <div class="offering-table-col col-mobile-links hidden-sm hidden-md hidden-lg">
        <?php
        if (get_sub_field("virtual_tour_url")) {
            ?>
            <p>
                <a href="<?php the_sub_field("virtual_tour_url"); ?>"
                    title="<?php _e('Take Virtual Tour', 'workspring'); ?>"
                    target="_blank">
                    <?php _e("Take Virtual Tour", "workspring"); ?>
                </a>
            </p>
            <?php
        } // endif virtual tour
        if (get_sub_field("floorplan")) {
            ?>
            <p>
                <a href="<?php the_sub_field("floorplan", "workspring"); ?>"
                    title="<?php _e('Floorplan', 'workspring'); ?>"
                    download
                    target="_blank">
                    <?php _e("View Floor Plan", "workspring"); ?>
                </a>
            </p>
            <?php
        } // endif floorplan ?>
    </div>
</div>
