<h2 class="sr-only">General</h2>

<div class="row">
    <div class="col-md-6">
        <div class="location-address <?php if (get_field("affiliate")) { echo 'affiliate-location-address';} ?>">
            <?php
            the_field("location_line_1");
            if (get_field("location_line_2")) {
                ?><br><?php
                the_field("location_line_2");
            }
            if (get_field("location_line_3")) {
                ?><br><?php
                the_field("location_line_3");
            } ?>
        </div>

        <?php
        if (get_field("quote")) {
            ?>
            <blockquote class="location-quote">
                <p>&ldquo;<?php the_field("quote"); ?>&rdquo;</p>
            </blockquote>
            <?php
        } // endif quote
        ?>

        <div>
            <?php
            if (!get_field("affiliate")) {
                ?>
                <a href="<?php the_field("booking_page", "options") ?>?book-location=<?php echo basename(get_permalink()); ?>"
                    class="btn btn-primary book-button" modal modal-iframe>
                    <?php _e('BOOK', 'workspring'); ?>
                </a>
                <?php
            } // endif not affiliate ?>

            <?php get_template_part("partials/sharing"); ?>
        </div>

        <?php get_template_part("location/general", "links"); ?>
    </div>

    <div class="col-md-6">
        <?php
        if (get_field("event_title")) {
            ?>
            <div class="location-event">
                <h3 class="sr-only">Upcoming Event</h3>
                <p class="location-event-title">
                    <strong><?php the_field("event_title"); ?></strong>
                </p>
                <p><?php the_field("event_excerpt"); ?></p>
                <p><a href="<?php echo SOP_locationEventUrl(); ?>">Read More</a></p>
                <p>
                    <a href="<?php echo get_permalink(get_option("page_for_posts")); ?>">
                        View More News &amp; Events at this Location
                    </a>
                </p>
            </div>
        <?php
        } // endif event ?>
    </div>

    <div class="col-md-6">
        <h3 class="small-heading">Hours</h3>
        <?php the_field("hours"); ?>

        <h3 class="small-heading">Contact</h3>
        <?php the_field("contact"); ?>

        <p>
            <a href="tel:+<?php echo SOP_cleanPhone(get_field("phone_number")); ?>">
                <?php the_field("phone_number"); ?>
            </a>
        </p>

        <p>
            <a href="mailto:<?php the_field("email"); ?>">
                <?php the_field("email"); ?>
            </a>
        </p>

        <?php get_template_part("location/social"); ?>
    </div>
</div>

<?php
if (get_field("tour_url")) {
    ?>
    <h3 class="large-heading"><?php _e('Tour the Space', 'workspring'); ?></h3>
    <?php
    if (get_field("tour_url")) {
        ?>
        <div class="embed-responsive embed-responsive-16by9 hidden-sm hidden-xs">
            <iframe width="720" height="540" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                adaptive-src="<?php echo SOP_getTourUrl(get_field("tour_url")); ?>"></iframe>
        </div>
        <?php
    } // endif tour url

    if (get_field("tour_image")) {
        ?>
        <a href="<?php the_field("tour_url"); ?>" target="_blank" class="hidden-md hidden-lg">
            <?php
            echo wp_get_attachment_image(
                get_field("tour_image"),
                "location-video",
                false,
                array("class" => "img-responsive center-block")
            ); ?>
        </a>
        <?php
    } else {
        ?>
        <div class="embed-responsive embed-responsive-16by9 hidden-md hidden-lg">
            <iframe width="720" height="540" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                adaptive-src="<?php echo SOP_getTourUrl(get_field("tour_url")); ?>"></iframe>
        </div>
        <?php
    } // endif tour image
} // endif tour

if (get_field("video_url")) {
    ?>
    <h3 class="large-heading"><?php the_field("video_title"); ?></h3>

    <?php
    if (get_field("video_image")) {
        ?>
        <a href="<?php the_field("video_url"); ?>" target="_blank" class="location-video-trigger" modal modal-video>
            <?php
            echo wp_get_attachment_image(
                get_field("video_image"),
                "location-video",
                false,
                array("class" => "img-responsive center-block")
            );
            ?>
        </a>
        <?php
    } // endif video image
} // endif video url
