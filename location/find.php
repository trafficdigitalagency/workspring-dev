<div class="container">
    <h2 class="sr-only"><?php _e('Find Us', 'workspring'); ?></h2>

    <div class="embed-responsive embed-responsive-16by9 hidden-xs">
        <iframe adaptive-src="<?php echo SOP_mapsEmebed(get_field("address")); ?>"
            width="720" height="540" frameborder="0" style="border:0"></iframe>
    </div>

    <div class="hidden-md hidden-lg hidden-sm">
        <div class="mobile-map-outer">
            <a href="<?php echo SOP_directionsUrl(get_field("address")) ?>" target="_blank">
                <img src="<?php echo SOP_staticMapUri(get_field("address"), 320, 240); ?>"
                    width="320" height="240" alt="Map"
                    class="img-responsive center-block">
            </a>
        </div>

        <a href="<?php echo SOP_directionsUrl(get_field("address")) ?>" target="_blank" class="btn btn-primary">
            <?php _e('GET DIRECTIONS', 'workspring'); ?>
        </a>
    </div>

    <div class="location-directions wysiwyg">
        <?php the_field("directions"); ?>
    </div>
</div>
