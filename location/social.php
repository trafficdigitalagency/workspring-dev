<?php
if (get_field("social")) {
    ?>
    <ul class="social-menu">
    <?php
    while (has_sub_field("social")) {
        ?>
        <li class="social-menu-item">
            <a href="<?php the_sub_field("url"); ?>"
                target="_blank"
                class="social-menu-link social-menu-<?php the_sub_field("type"); ?>">
                <?php the_sub_field("type"); ?>
            </a>
        </li>
        <?php
    } // while social
    ?>
    </ul>
    <?php
} // endif social
