<h2 class="sr-only"><?php echo get_sub_field("type")->post_title; ?></h2>

<?php
if (get_sub_field("slider")) {
    ?>
    <div class="slides location-offering-slider" slick="location-offering">
    <?php
    while (has_sub_fields("slider")) {
        ?>
        <div class="slide">
            <?php
            echo wp_get_attachment_image(
                get_sub_field("image"),
                "location-video",
                false,
                array("class" => "img-responsive center-block")
            ); ?>

            <div class="slide-caption">
                <?php the_sub_field("caption"); ?>
            </div>
        </div>
        <?php
    } // endwhile slider
    ?>
    </div>
    <?php
} // endif slider
?>

<div class="container">
    <?php
    if (get_sub_field("description")) {
        ?>
        <blockquote class="location-offering-quote">
            <p><?php the_sub_field("description"); ?></p>
        </blockquote>
        <?php
    } // endif description

    ?>
    <div class="wysiwyg">
        <?php the_sub_field("additional_info"); ?>
    </div>
    <?php

    if (!get_field("affiliate")) {
        $GLOBALS['post'] = get_sub_field("type");
        setup_postdata($GLOBALS['post']);

        SOP_bootstrapOfferings();
        ?>
        <div class="offerings location-offering">
            <?php
            get_template_part("offerings/offering");
            ?>
        </div>
        <?php
        wp_reset_postdata();
        // get_template_part("partials/offerings");
    }

    if (get_sub_field("space_details")) {
        get_template_part("location/offering", "space");
    } // endif space details
    ?>
</div>
