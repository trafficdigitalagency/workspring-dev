<?php
/**
 * Location Offerings Tab Contents
 */

if (get_field("offerings")) {
    while (has_sub_field("offerings")) {
        ?>
        <div class="tab-content" tab="<?php echo get_sub_field("type")->post_name; ?>">
        <?php
        get_template_part("location/offering");
        ?>
        </div>
        <?php
    } // endwhile offerings
} // endif offerings
