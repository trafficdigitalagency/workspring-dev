<?php
/**
 * Location Guide Link
 */

if (get_sub_field("links")) {
    while (has_sub_field("links")) {
        ?>
        <li class="col-md-4 col-sm-6">
            <a href="<?php the_sub_field("url"); ?>" target="_blank">
                <?php the_sub_field("label"); ?>
            </a>
        </li>
        <?php
    }
} // endif links
