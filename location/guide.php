<h2 class="sr-only"><?php _e('Local Guide', 'workspring'); ?></h2>

<?php
if (get_field("categories")) {
    while (has_sub_field("categories")) {
        get_template_part("location/guide", "links");
    }
} // endif categories
