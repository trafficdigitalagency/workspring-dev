<?php
/**
 * Location Local Guide Links
 */
?>
<div class="location-guide-category">
    <h3 class="large-heading"><?php the_sub_field("name"); ?></h3>

    <div class="row">
        <ul class="location-guide-links">
            <?php get_template_part("location/guide", "link"); ?>
        </ul>
    </div>
</div>
<?php
