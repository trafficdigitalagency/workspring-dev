<?php
/**
 * Location Offering Space Details
 */
?>
<h3 class="large-heading">Space Details</h3>

<div class="table offering-table">
    <div class="table-header">
        <div class="row">
            <div class="offering-table-col col-name">
                <?php _e('Space Name', 'workspring'); ?>
            </div>
            <div class="offering-table-col col-capacity">
                <?php _e('Capacity', 'workspring'); ?>
            </div>
            <div class="offering-table-col col-dimensions">
                <?php _e('Dimensions', 'workspring'); ?>
            </div>
            <div class="offering-table-col col-tools">
                <?php _e('Tools &amp; Technology', ''); ?>
            </div>
            <div class="offering-table-col col-desktop-floorplan">
                <?php _e('Floorplan', 'workspring'); ?></a>
            </div>
        </div>
    </div>

    <div class="table-body">
        <?php
        do_action(
            "someoddpilot\LoopTemplate\loop",
            "location/offering-space-row",
            "has_sub_field",
            array("space_details")
        );
        ?>
    </div>
</div>
