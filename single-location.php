<?php
/**
 * Single Location Template
 */

get_template_part("partials/head");
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
        <div tabs>
            <div class="page-header" <?php echo SOP_backgroundImage(get_post_thumbnail_id(), 'page-header'); ?>>
                <h1 class="header-title"><?php the_title(); ?></h1>
                <nav class="hidden-sm hidden-xs">
                    <h2 class="sr-only"><?php _e('Location Menu', 'workspring'); ?></h2>
                    <ul class="location-nav">
                        <?php echo SOP_locationTabs(); ?>
                    </ul>
                </nav>
                <div class="header-dropdown-menu">
                    <div class="dropdown hidden-md hidden-lg">
                        <a href="javascript:void(0)"
                            class="btn btn-secondary" data-toggle="dropdown">
                            <?php the_title(); ?>
                        </a>
                        <ul class="dropdown-menu">
                            <?php echo SOP_locationTabs(); ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-contents">
                <div class="tab-content" tab="general">
                    <div class="container">
                        <?php get_template_part('location/general'); ?>
                    </div>
                </div>
                <?php
                get_template_part("location/offerings");
                if (get_field("categories") && ! get_field("affiliate")) {
                    ?>
                    <div class="tab-content" tab="local-guide">
                        <div class="container">
                            <?php get_template_part('location/guide'); ?>
                        </div>
                    </div>
                <?php
                } // endif  ?>
                <div class="tab-content" tab="find-us">
                    <div class="container">
                        <?php get_template_part('location/find'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
get_template_part("partials/foot");
