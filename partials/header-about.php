<?php
/**
 * About Header Template
 */
?>
<h1 class="sr-only"><?php the_title(); ?></h1>
<header class="page-header" <?php echo SOP_backgroundImage(get_field("about_header_image", "options"), 'page-header'); ?>>
    <div class="header-title">
        <?php _e('About Workspring', 'workspring'); ?>
    </div>
    <div class="hidden-sm hidden-xs">
        <?php wp_nav_menu(WSP_aboutNavArgs()); ?>
    </div>
    <div class="header-dropdown-menu">
        <div class="dropdown hidden-md hidden-lg">
            <a href="javascript:void(0)"
                class="btn btn-secondary" data-toggle="dropdown">
                <?php
                    if (is_post_type_archive('person')) {
                        echo 'People';
                    } else {
                        the_title();
                    }
                ?>
            </a>
            <?php
            wp_nav_menu(
                WSP_aboutNavArgs(
                    array(
                        'menu_class' => 'dropdown-menu',
                    )
                )
            ); ?>
        </div>
    </div>
</header>
