<?php
if (is_singular("post") || get_field('show_sharing')) {
    if (is_singular("post")) {
        get_template_part("partials/post-author");
    }

    ?>
    <time>
        <?php echo get_the_date('F j, Y'); ?>
    </time>
    <div class="sharing-outer">
        <?php get_template_part("partials/sharing"); ?>
    </div>
    <?php
} // endif
