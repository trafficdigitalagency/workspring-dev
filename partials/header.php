<header class="header-primary">
    <div class="header-top">
        <div class="container">
            <nav>
                <h2 class="sr-only">
                    <?php _e('Secondary Menu', 'workspring'); ?>
                </h2>
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'secondary',
                        'container'      => false,
                        'menu_class'     => 'secondary-menu',
                        'depth'          => 1,
                    )
                ); ?>
            </nav>
        </div>
    </div>
    <div class="container">
        <div class="header-bottom">
            <div class="header-bottom-main">
                <a href="<?php echo site_url(); ?>" class="logo-link">
                    <img src="<?php echo SOP_imageUri("logo-workspring.svg"); ?>"
                        class="logo-image"
                        width="213" height="57" alt="Workspring">
                </a>
                <div class="mobile-controls">
                    <a href="tel:+18006059092" class="mobile-menu-button phone">
                        <span class="sr-only">(800) 605-9092</span>
                    </a>
                    <a href="javascript:void(0);" class="mobile-menu-button menu"
                        data-toggle="collapse" data-target="#primary-menu">
                        <span class="sr-only">Primary Menu</span>
                    </a>
                </div>
            </div>
            <nav id="primary-menu" class="collapse primary-menu-outer clearfix" collapse>
                <h2 class="sr-only">
                    <?php _e('Primary Menu', 'workspring'); ?>
                </h2>
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'primary',
                        'container'      => false,
                        'menu_class'     => 'primary-menu',
                        'depth'          => 2,
                    )
                ); ?>
            </nav>
        </div>
    </div>
</header>
