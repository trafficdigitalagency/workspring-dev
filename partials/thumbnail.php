<?php
if (has_post_thumbnail()) {
    ?>
    <div class="featured-image-outer">
        <?php
        if (get_field("featured_video")) {
            ?>
            <a href="<?php the_field("featured_video"); ?>" modal modal-video class="video-link">
            <?php
        }

        the_post_thumbnail();

        if (get_field("featured_video")) {
            ?>
            </a>
            <?php
        }

        $attachment = get_post(
            get_post_thumbnail_id()
        );

        if (!empty($attachment->post_excerpt)) {
            echo "<div class='wp-caption'><p>" . $attachment->post_excerpt . "</p></div>";
        }
        ?>
    </div>
    <?php
}
