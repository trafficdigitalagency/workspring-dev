<?php
get_template_part("partials/head");
?>
<div class="container">
    <div class="about-listing-header">
        <h2><?php echo get_the_title(get_option("page_for_posts")); ?></h2>
    </div>

    <div class="row">
        <?php
        SOP_locationFilter();

        SOP_taxomonyFilter("category", "Filter by Category");
        ?>
    </div>

    <?php
    if (!empty($_GET['location-id'])) {
        query_posts(
            wp_parse_args(
                array(
                    "connected_type"  => "posts_to_locations",
                    "connected_items" => intval($_GET['location-id']),
                ),
                $wp_query->query_vars
            )
        );
    }


    $sticky = get_option('sticky_posts');

    if (!empty($sticky) && is_home()) {
        $stickyQueryVars = wp_parse_args(
            array(
                "post__in"            => $sticky,
                "posts_per_page"      => 1,
                "ignore_sticky_posts" => true,
            ),
            $wp_query->query_vars
        );


        if (!empty($_GET['category-id'])) {
            $stickyQueryVars = wp_parse_args(
                array(
                    "tax_query" => array(
                        array(
                            "taxonomy" => "category",
                            "field"    => "term_id",
                            "terms"    => $_GET["category-id"]
                        )
                    )
                ),
                $stickyQueryVars
            );
        }

        $stickyQuery = new WP_Query($stickyQueryVars);

        if ($stickyQuery->have_posts()) {
            while ($stickyQuery->have_posts()) {
                $stickyQuery->the_post();

                get_template_part("partials/listing-featured");
            }
        }
    }

    $query_args = array();

    if (!empty($stickyQuery)) {
        $query_args["post__not_in"] = array($stickyQuery->post->ID);
    }

    if (!empty($_GET['location-id'])) {
        $query_args["connected_type"]  = "posts_to_locations";
        $query_args["connected_items"] = intval($_GET['location-id']);
    }

    if (!empty($_GET['category-id'])) {
        $query_args = wp_parse_args(
            array(
                "tax_query" => array(
                    array(
                        "taxonomy" => "category",
                        "field"    => "term_id",
                        "terms"    => $_GET["category-id"]
                    )
                )
            ),
            $query_args
        );
    }

    $query_args["ignore_sticky_posts"] = true;

    query_posts(
        wp_parse_args(
            $query_args,
            $wp_query->query_vars
        )
    );

    get_template_part('partials/listing'); ?>
</div>
<?php

get_template_part("partials/foot");
