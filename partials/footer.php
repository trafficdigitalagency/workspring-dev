<footer class="footer-primary">
    <div class="container">
        <div class="row flexbox-column">
            <nav class="col-sm-6 flex-3">
                <h2 class="sr-only">Tertiary Menu</h2>
                <div class="footer-info">
                    <p class="copyright">&copy; <?php echo date('Y'); ?> Steelcase</p>
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'footer',
                            'container' => false,
                            'menu_class' => 'footer-menu',
                        )
                    ); ?>
                </div>
            </nav>
            <nav class="col-sm-3 footer-social-outer">
                <h2 class="sr-only">Social</h2>
                <?php
                if (have_rows("social", "options")) {
                    ?>
                    <ul class="social-menu">
                    <?php
                    while (have_rows("social", "options")) {
                        the_row();
                        ?>
                            <li class="social-menu-item">
                                <a href="<?php the_sub_field("url"); ?>" target="_blank"
                                    class="social-menu-link social-menu-<?php the_sub_field("type"); ?>">
                                    <?php the_sub_field("type"); ?>
                                </a>
                        </li>
                        <?php
                    }
                    ?>
                    </ul>
                    <?php
                }
                ?>
            </nav>
            <div class="col-sm-3 footer-signup-outer">
                <form id="command" class="footer-newsletter-form" target="_blank"
                    action="http://visitor.r20.constantcontact.com/manage/optin/ea?v=00181hjlAJ2FDbeXcOV08WkjhNvOgblX4pyzL5A26u6uUivXMJtjOIuTS0CJ7VbQN42zcB9e56QpIc%3D" method="post">
                    <input type="hidden" name="ref" value="http://www.workspring.com/">

                    <input type="email" class="footer-newsletter-input"
                        required
                        name="emailAddr"
                        placeholder="Newsletter Sign Up">

                    <button name="_save" value="Continue" class="footer-newsletter-button">
                        Subscribe
                    </button>
                </form>
            </div>
        </div>
    </div>
</footer>
