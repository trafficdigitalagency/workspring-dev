<?php
/**
 * Footer template
 */

if (!SOP_isiFrame()) {
    get_template_part("partials/footer");
} // endif not iframe
wp_footer();
?>
        <script>
<?php
foreach (SOP_localizeScript() as $key => $value) {
    echo 'var ' . $key . ' = ' . json_encode($value) . ';';
}
?>
        </script>
        <script async data-main="<?php echo SOP_mainUri(); ?>"
            src="<?php echo site_url(); ?>/wp-content/themes/workspring/js/vendor/require<?php echo SOP_fileSuffix() ?>.js"></script>
    </body>
</html>
