<?php
if (has_post_thumbnail()) {
    ?>
    <a <?php echo SOP_featuredImageLinkAttr(); ?>>
        <?php the_post_thumbnail('news-image'); ?>
    </a>
    <?php
} // endif ?>
<h3 class="medium-heading">
    <?php the_title(); ?>
</h3>
<?php
if (is_home()) {
    ?>
    <time>
        <?php echo get_the_date("F j, Y"); ?>
    </time>
    <?php
}

the_excerpt(); ?>
<a class="read-more" href="<?php the_permalink(); ?>">
    <?php _e('Read More', 'workspring'); ?>
</a>
