<?php
if (get_field("author")) { ?>
    <div>By <a href="<?php echo get_permalink(get_field("author")->ID); ?>">
        <?php echo get_the_title(get_field("author")->ID); ?>
    </a></div>
    <?php
} // endif author
