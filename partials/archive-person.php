<?php

get_template_part("partials/head");

get_template_part("partials/header", "about");
?>
<div class="container">
    <div class="about-listing-header">
        <div class="row">
            <div class="col-md-12">
                <h2>People</h2>
            </div>
            <?php
            SOP_locationFilter();

            SOP_taxomonyFilter("role", "Filter by Role");
            ?>
            <div class="col-md-4">
                <p><?php the_field("people_subcopy", "options"); ?></p>
            </div>
        </div>
    </div>

    <?php

    $queryModifications = array(
        "orderby" => "menu_order",
        "order"   => "DESC",
    );

    if (!empty($_GET["role-id"])) {
        $queryModifications = wp_parse_args(
            array(
                "tax_query" => array(
                    array(
                        "taxonomy" => "role",
                        "field"    => "term_id",
                        "terms"    => $_GET["role-id"]
                    )
                )
            ),
            $queryModifications
        );
    }

    if (!empty($_GET["location-id"])) {
        $queryModifications = wp_parse_args(
            array(
                "connected_type"  => "person_to_locations",
                "connected_items" => intval($_GET["location-id"]),
            ),
            $queryModifications
        );
    }

    query_posts(
        wp_parse_args(
            $queryModifications,
            $GLOBALS["wp_query"]->query_vars
        )
    );

    get_template_part("partials/listing"); ?>
</div>
<?php
get_template_part("partials/foot");
