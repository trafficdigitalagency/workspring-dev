<?php
/**
 * Header Template
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6632072/612264/css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" />
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/html5shiv.min.js"></script>
    <![endif]-->
    <?php get_template_part("partials/favicons"); ?>
    <?php wp_head(); ?>
    <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-4877355-1', 'workspring.com');
ga('send', 'pageview');
    </script>
    <?php
    if( is_page('lander') ){ ?>
          <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/dist/css/lander.css" />
    <?php }
    ?>
</head>
<body <?php body_class(); ?>>
    <div id="page" class="hfeed site">
        <?php
        if (! SOP_isiFrame()) {
            get_template_part("partials/header");
        } // endif not iframe
