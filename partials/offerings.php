<div class="container">
    <div class="row">
        <h2 class="sr-only">Offerings</h2>

        <?php
        $offerings = new WP_Query(
            array(
                "post_type"      => "offering",
                "posts_per_page" => 4,
            )
        );

        if ($offerings->have_posts()) {
            ?>
            <div class="offerings has-<?php echo $offerings->post_count; ?>-offerings">
            <?php
            while ($offerings->have_posts()) {
                $offerings->the_post();

                get_template_part("offerings/offering");
            }
            ?>
            </div>
            <?php
        }

        wp_reset_postdata();
        ?>
    </div>
</div>
