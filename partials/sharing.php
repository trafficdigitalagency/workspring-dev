<a href="javascript:void(0)" class="btn btn-default"
    data-toggle="popover"
    data-html="true"
    data-placement="top"
    data-content="<?php echo htmlspecialchars(SOP_getTemplateString("partials/sharing-links")); ?>">
    <?php _e('SHARE', 'workspring'); ?>
</a>
