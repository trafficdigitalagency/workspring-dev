<ul class="post-sharing">
    <li class="sharing-item linkedin">
        <a href="<?php echo SOP_linkedInShareUrl( get_permalink(), get_the_title(), get_the_excerpt() ); ?>"
            target="_blank" rel="nofollow">
            <span class="sr-only">LinkedIn</span>
        </a>
    </li>
    <li class="sharing-item twitter">
        <a href="<?php echo SOP_twitterShareUrl( get_permalink(), get_the_title() ); ?>"
            target="_blank" rel="nofollow">
            <span class="sr-only">Twitter</span>
        </a>
    </li>
    <li class="sharing-item facebook">
        <a href="<?php echo SOP_facebookShareUrl( get_permalink() ); ?>"
            target="_blank" rel="nofollow">
            <span class="sr-only">Facebook</span>
        </a>
    </li>
</ul>
