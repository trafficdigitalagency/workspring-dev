<?php

global $wp_query;

if (have_posts()) {
    ?>
    <div sop-waterfall>
        <div class="row about-listing">
        <?php

        while (have_posts()) {
            the_post();
            ?>
            <div class="col-md-4 listing-item <?php echo SOP_nthChildClass(SOP_getLoopIndex()); ?>">
                <?php get_template_part("partials/listing-item"); ?>
            </div>
            <?php
        } // endwhile posts
        ?>
        </div>
        <?php
        global $wp_query;
        $big = 999999999; // need an unlikely integer

        ?>
        <div class="pagination">
        <?php
        echo paginate_links(
            array(
                'base'    => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format'  => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'total'   => $wp_query->max_num_pages,
            )
        );
        ?>
        </div>
    </div>
    <?php
} else {
    ?>
    <h2 class="large-heading">Sorry, no posts found.</h2>
    <p>Please try another category.</p>
    <?php
}// endif posts
