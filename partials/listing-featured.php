<div class="row listing-featured">
    <div class="col-md-8">
        <a <?php echo SOP_featuredImageLinkAttr(); ?>>
            <?php the_post_thumbnail('large-news-image'); ?>
        </a>
    </div>
    <div class="col-md-4">
        <h3 class="large-heading">
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h3>
        <time>
            <?php echo get_the_date("F j, Y"); ?>
        </time>
        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="btn btn-primary">READ MORE</a>
        <?php get_template_part("partials/sharing"); ?>
    </div>
</div>
