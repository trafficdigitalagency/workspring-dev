<?php
if (is_singular("post") || get_field('show_sharing')) {
    echo get_the_category_list(", ");
    ?>
    <div class="post-footer">
        <?php get_template_part("partials/sharing"); ?>

        <a href="<?php echo SOP_backUrl(); ?>" class="btn btn-primary">BACK</a>
    </div>
    <?php
} // endif
