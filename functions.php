<?php

use someoddpilot\PostType;
use someoddpilot\GForm;
use someoddpilot\ACF;
use someoddpilot\Template;

$requiredMUPlugins = array(
    '/acf/acf.php',
    '/acf/acf.php',
    '/acf-options-page/acf-options-page.php',
    '/acf-repeater/acf-repeater.php',
    '/acf-wp-wysiwyg/acf-wp_wysiwyg.php',
    '/gf-acf/acf-gravity_forms.php',
    '/gravityforms/gravityforms.php',
    '/posts-to-posts/posts-to-posts.php',
    '/gravity-forms-placeholders/gravityforms-placeholders.php',
);

foreach ($requiredMUPlugins as $muPlugin) {
    require_once WPMU_PLUGIN_DIR . $muPlugin;
}

$includedFunctions = array(
    '/vendor/autoload.php',
    '/inc/menus.php',
    '/inc/admin.php',
    '/inc/sharing.php',
    '/inc/conditionals.php',
    '/inc/assets.php',
    '/inc/fields.php',
    '/inc/tags.php',
    '/inc/caching.php',
    '/inc/sorting.php',
    '/inc/waterfall.php',
);

foreach ($includedFunctions as $includedFunction) {
    require_once __DIR__ . $includedFunction;
}

remove_shortcode('gallery', 'gallery_shortcode');
add_shortcode('gallery', function ($params) {
    $images = explode(',', $params['ids']);
    return '<div class="slides slides-content" slick="content">' . array_reduce($images, function ($memo, $imageId) {
        $image = get_post($imageId);
        list($src) = wp_get_attachment_image_src($imageId, 'post-slider');
        return $memo . '<div class="slide">'
            . '<img class="img-responsive" src="' . $src . '">'
            . '<div><h3>'
            . apply_filters('the_title', $image->post_title)
            . '</h3>'
            . apply_filters('the_content', $image->post_content)
            . '</div>'
            . '</div>';
    }) . '</div>';
});

add_action("init", function () {
    $labels = array(
        'name'                  => __('Roles', 'workspring'),
        'singular_name'         => __('Role', 'workspring'),
        'search_items'          => __('Search Roles', 'workspring'),
        'popular_items'         => __('Popular Roles', 'workspring'),
        'all_items'             => __('All Roles', 'workspring'),
        'parent_item'           => __('Parent Role', 'workspring'),
        'parent_item_colon'     => __('Parent Role', 'workspring'),
        'edit_item'             => __('Edit Role', 'workspring'),
        'update_item'           => __('Update Role', 'workspring'),
        'add_new_item'          => __('Add New Role', 'workspring'),
        'new_item_name'         => __('New Role Name', 'workspring'),
        'add_or_remove_items'   => __('Add or remove Roles', 'workspring'),
        'choose_from_most_used' => __('Choose from most used Roles', 'workspring'),
        'menu_name'             => __('Role', 'workspring'),
    );

    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'show_in_nav_menus' => false,
        'show_admin_column' => false,
        'hierarchical'      => true,
        'show_tagcloud'     => true,
        'show_ui'           => true,
        'query_var'         => true,
        'rewrite'           => true,
        'query_var'         => true,
        'capabilities'      => array(),
    );

    register_taxonomy(
        'role',
        array('person'),
        $args
    );
});

add_action('init', function () {
    add_post_type_support('page', 'excerpt');
});

add_filter("excerpt_more", function ($more) {
    return "&hellip;";
});

$fields = array(
    "about-header-image",
    "booking",
    "contact",
    "faq",
    "friends-of-workspring",
    "front-page",
    "location-event",
    "location-find-us",
    "location-general",
    "location-local-guide",
    "location-offerings",
    "location-social",
    "offering-booking-info",
    "offering-location-pricing",
    "offerings",
    "pages",
    "people",
    "location-search",
    "post",
    "pricing-and-memberships",
    "show-social",
    "social",
    "why-workspring",
);

foreach ($fields as $name) {
    SOP_registerFieldGroup($name);
}

$location = new PostType\Location();

$offering = new PostType\Offering();

$person = new PostType\Person();

$gFormSkipPage = new GForm\SkipPage();

$addressComponents = new ACF\AddressComponents();

$loopTemplate = new Template\LoopTemplate();

add_theme_support('post-thumbnails');

SOP_addImageSizes();

add_action('init', function () {
    add_editor_style('css/editor.css');
    add_editor_style('//cloud.typography.com/6632072/612264/css/fonts.css');
});

add_theme_support('admin-bar', array('callback' => '__return_false'));

add_action('admin_menu', 'SOP_disableDashboardWidgets');

add_action('init', 'WSP_registerMenus');

add_action('wp_enqueue_scripts', 'WSP_enqueue');

add_action('wp_ajax_nopriv_waterfall', 'WSP_waterfall');
add_action('wp_ajax_waterfall', 'WSP_waterfall');

if (!empty($_GET['iframe'])) {
    add_filter('show_admin_bar', '__return_false');
}

add_action('p2p_init', function () {
    if (function_exists("p2p_register_connection_type")) {
        $connections = array(
            array(
                'name'       => 'person_to_locations',
                'from'       => 'person',
            ),
            array(
                'name'       => 'pages_to_locations',
                'from'       => 'page',
            ),
            array(
                'name'       => 'posts_to_locations',
                'from'       => 'post',
                'reciprocal' => true,
            )
        );

        foreach ($connections as $connection) {
            p2p_register_connection_type(
                wp_parse_args(
                    $connection,
                    array("to" => "location")
                )
            );
        }
    }
});

add_filter('gform_previous_button', function ($button) {
    return str_replace("value='Previous'", "value='" . __("Back", "workspring") . "'", $button);
});
