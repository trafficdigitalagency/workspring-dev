<?php
/**
 * Template Name: Location Search
 */

get_template_part("partials/head");

?>
<div class="container">
    <div class="content-container">
        <h1>Location Search</h1>
        <form class="form gform_wrapper">
            <div class="form-group">
                <input type="text" name="address" value="<?php echo htmlspecialchars($_GET['address']); ?>" placeholder="Your Address">
            </div>
        </form>
        <?php
        $locationAddress = new someoddpilot\LocationAddress\LocationAddress($_GET['address']);

        $locationSearch = new someoddpilot\LocationAddress\LocationSearch($locationAddress);

        foreach ($locationSearch->locations as $location) {
            ?>
            <li>
                <a href="<?php echo get_permalink($location->ID); ?>">
                    <?php
                    echo $location->post_title;
                    if ($location->distance < 6000) {
                        echo ' &mdash; ' . round($location->distance, 2) . ' miles';
                    } ?>
                </a>
            </li>
            <?php
        }
    ?>
    </div>
</div>
<?php

get_template_part("partials/foot");
