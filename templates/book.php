<?php
/**
 * Template Name: Book
 */

SOP_loop(function () {
    ?>
    <div class="container booking-form">
        <div class="clearfix">
            <?php the_post_thumbnail("concierge", array("class" => "concierge-image pull-left img-circle")); ?>

            <?php $steps = get_field("steps"); ?>
            <blockquote class="pull-left">
                <p class="concierge-info"><?php echo $steps[SOP_getGformPage()]['concierge_info']; ?></p>
            </blockquote>
        </div>

        <div class="row">

            <div class="col-sm-6 wysiwyg">
                <h2><?php echo SOP_getBookHeading(); ?></h2>

                <?php echo SOP_getBookInfo(); ?>
            </div>

            <div class="col-sm-6">
                <?php SOP_enqueueForm(); ?>
            </div>
        </div>
    </div>
    <?php
});
