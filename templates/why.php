<?php
/**
 * Template Name: Why Workspring?
 */

SOP_loop(function () {
    get_template_part('partials/header', 'about');
    ?>
    <div class="container">
        <div class="about-listing-header">
            <div class="row">
                <div class="col-md-6">
                    <h2><?php the_field("heading"); ?></h2>
                </div>
                <div class="col-md-6">
                    <div class="about-subheader">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php get_template_part("about/listing"); ?>
    </div>
    <?php
});
