<?php
/**
 * Template Name: Thank You
 */

SOP_loop(function () {
    ?>
    <script type="text/javascript"> new Image().src = '//clickserv.pixel.ad/conv/f44c346e9331925f'; </script>
    <div class="container booking-form">
        <div class="clearfix">
            <?php the_post_thumbnail("concierge", array("class" => "concierge-image pull-left img-circle")); ?>

            <?php $steps = get_field("steps"); ?>
            <blockquote class="pull-left">
                <p class="concierge-info"><?php echo $steps[SOP_getGformPage()]['concierge_info']; ?></p>
            </blockquote>
        </div>

        <div class="row">

            <div class="col-md-8 wysiwyg">
                <h2><?php the_field('book_thank_you_heading'); ?></h2>

                <?php the_field('book_thank_you_body'); ?>
            </div>

        </div>
    </div>
    <?php
});
