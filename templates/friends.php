<?php
/**
 * Template Name: About Section Listing
 */

SOP_loop(function () {
    get_template_part('partials/header', 'about');
    ?>
    <div class="container">
        <div class="about-listing-header">
            <div class="row">
                <div class="col-md-6">
                    <h2><?php the_field("heading"); ?></h2>
                    <?php
                    SOP_loopOverLocations(function () {
                        ?>
                        <div class="dropdown">
                            <a href="javascript:void(0)"
                                class="btn btn-secondary" data-toggle="dropdown">
                                Filter by location
                            </a>
                            <ul class="dropdown-menu">
                                <?php
                                while (have_posts()) {
                                    the_post();
                                    ?>
                                    <li class="dropdown-item">
                                        <a href="?filter-location=<?php the_id(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </li>
                                    <?php
                                } // endwhile
                                ?>
                            </ul>
                        </div>
                        <?php
                    });
                    ?>
                </div>
                <div class="col-md-6">
                    <div class="about-subheader">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php get_template_part("about/listing"); ?>
    </div>
    <?php
});
