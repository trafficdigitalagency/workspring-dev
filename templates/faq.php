<?php
/**
 * FAQ Template
 *
 * Template Name: FAQ
 */

SOP_loop(function () {
    get_template_part('partials/header', 'about');
    ?>
    <div class="container">
        <div class="content-container">
            <?php
            if (have_rows("sections")) {
                ?>
                <?php
                while (have_rows("sections")) {
                    the_row();
                    $categoryTitle = get_sub_field("title");
                    ?>
                    <h2><?php the_sub_field("title"); ?></h2>
                    <?php
                    if (have_rows("questions")) { ?>
                    <div class="entry-content">
                        <dl class="faq-list">
                            <?php
                            while (have_rows("questions")) {
                                the_row();
                                ?>
                                <dt data-toggle="collapse" data-target="#answer-<?php echo sanitize_title($categoryTitle) . '-' . SOP_getRowIndex(); ?>">
                                    <?php the_sub_field("question") ?>
                                </dt>
                                <dd id="answer-<?php echo sanitize_title($categoryTitle) . '-' . SOP_getRowIndex(); ?>">
                                    <?php the_sub_field("answer") ?>
                                </dd>
                                <?php
                            } // endwhile questions ?>
                        </dl>
                    </div>
                    <?php
                    } // endif questions
                } // endwhile sections
            } // endif sections
            ?>
        </div>
    </div>
    <?php
});
