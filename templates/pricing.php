<?php
/**
 * Pricing Template
 *
 * Template Name: Pricing and Memberships
 */

SOP_loop(function () {
    ?>
    <div class="page-header" <?php echo SOP_backgroundImage(get_post_thumbnail_id(), 'page-header'); ?>>
        <h1 class="header-title"><?php the_title(); ?></h1>
    </div>

    <div class="container">
        <div class="pricing-content-container">
            <div class="entry-content wysiwyg">
                <?php the_content(); ?>
            </div>
        </div>
    </div>

    <?php get_template_part('partials/offerings'); ?>
    <div class="container">
        <h2><?php _e('Promotions &amp; Loyalty', 'workspring'); ?></h2>

        <div class="row">
            <?php get_template_part("about/listing"); ?>
        </div>
    </div>
    <?php
});
