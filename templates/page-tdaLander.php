<?php
/**
 * Template Name: Lander Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

SOP_loop(function () {
    ?>

    <div id="lander_wrapper">
      <section class="video_section">
        <div class="headline">
          <?php
          $hero_headline = get_field('hero_headline');
          $hero_text = get_field('hero_text'); ?>

          <?php if( $hero_headline ): ?>
              <h2>
                <?php echo $hero_headline; ?>
              </h2>
          <?php endif; ?>

          <?php if( $hero_text ): ?>
            <p>
              <?php echo $hero_text; ?>
            </p>
          <?php endif; ?>
        </div>

        <div id="videos" class="videos">
          <div class="video-modals">
            <div class="tda-modal-fade">
              <div class="tda-modal-inner">
                <div class="video-slider">
            <?php if( have_rows('videos_listing') ): ?>
              <?php
              $y = 0;
              while( have_rows('videos_listing') ): the_row();

                // vars
                $video_id = get_sub_field('video_vimeo_id');

                ?>

                  <div class="video-slide">
                    <iframe id="video<?php echo $y; ?>" src="https://player.vimeo.com/video/<?php the_sub_field('video_vimeo_id'); ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                  </div>

              <?php
              $y++;
            endwhile; ?>
            <?php endif; ?>
                </div>
                  <a href="<?php echo site_url(); ?>/lander-contact-us" class="aButton">Contact Us</a>
                <div class="tda-modal-close" for="tda-modal"></div>
              </div>
            </div>
          </div>
          <div class="desktop">

            <?php if( have_rows('videos_listing') ): ?>

            	<?php
              $x = 0;
              while( have_rows('videos_listing') ): the_row();

            		// vars
            		$video_title = get_sub_field('video_title');
            		$video_image = get_sub_field('video_image');

            		?>

                <div id="<?php echo $x; echo $x; ?>" class="select-video" style="background-image: url(<?php echo $video_image; ?>); background-size: cover; background-position: center; background-repeat: no-repeat;">
                  <div class="tda-modal">
                    <label class="video-tda-modal" for="<?php echo $x; ?>">
                      <div class="tda-modal-trigger">
                        <h4><?php echo $video_title; ?></h4>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/play-button.png">
                      </div>
                    </label>
                    <input class="tda-modal-state tda-trig" id="<?php echo $x; ?>" type="checkbox" />

                  </div>
                </div>

            	<?php $x++; endwhile; ?>

            <?php endif; ?>
          </div>

          <div class="button">
            <?php //the_field('name_of_field'); ?>
            <a href="#research" class="aButton">The Research</a>
          </div>
          <div class="button">
            <a href="<?php echo site_url(); ?>/lander-contact-us" class="aButton">Contact Us</a>
          </div>
        </div>

      </section>
      <section class="about_section">
        <div class="line-top"></div>

        <div class="presenter">
          <?php
          $about_img = get_field('about_image');
          $about_headline = get_field('about_headline');
          $about_text = get_field('about_text'); ?>

          <div class="image">

            <?php if( $about_img ): ?>
              <img src="<?php echo $about_img; ?>">
            <?php endif; ?>

          </div>
          <div class="text-wrapper">
            <?php if( $about_headline ): ?>
                <h2>
                  <?php echo $about_headline; ?>
                </h2>
            <?php endif; ?>

            <?php if( $about_text ): ?>
              <p>
                <?php echo $about_text; ?>
              </p>
            <?php endif; ?>

            <a href="#lander_wrapper" class="aButton">Back to Videos</a>
          </div>
        </div>

        <div class="line-bottom"></div>
      </section>


      <section id="research" class="info_section">

        <div class="researchHero">
          <p>The Research</p>
        </div>
        <div class="research_headline">
          <?php
          $research_head_text = get_field('research_head_text'); ?>

          <?php if( $research_head_text ): ?>
            <p>
              <?php echo $research_head_text; ?>
            </p>
          <?php endif; ?>
        </div>

        <?php if( have_rows('research_information') ): ?>
        	<?php while( have_rows('research_information') ): the_row();

        		// vars
        		$research_image = get_sub_field('research_image');
        		$research_headline = get_sub_field('research_headline');
            $research_text = get_sub_field('research_text');

            // echo $var;

        		?>
            <div class="innovation">
              <div class="image">
                <img src="<?php echo $research_image; ?>">
              </div>
              <div class="text-wrapper">
                <h2> <?php echo $research_headline; ?> </h2>
                <p>
                  <?php echo $research_text; ?>
                </p>
              </div>
              <div class="buttonWrapper">
                <div class="button-read">
                  <a href="#" class="pdf-trigger aButton">Read </a>
                </div>
                <div class="button">
                  <a href="#lander_wrapper" class="aButton">Back to videos</a>
                </div>
              </div>

            </div>


        	<?php endwhile; ?>
        <?php endif; ?>

      </section>
      <script src="<?php echo get_template_directory_uri(); ?>/dist/js/slick.min.js"/></script>
      <script src="<?php echo get_template_directory_uri(); ?>/dist/js/video.js"/></script>
      <script src="<?php echo get_template_directory_uri(); ?>/dist/js/main.js" type="text/javascript"/></script>
  </div>

    <?php
});
