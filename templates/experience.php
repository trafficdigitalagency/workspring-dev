<?php
/**
 * Template Name: Experience
 */

SOP_loop(function () {
    get_template_part('partials/header', 'about');
    ?>
    <div class="container">
        <div class="content-container">
            <h1 class="sr-only"><?php the_title(); ?></h1>
            <div class="entry-content wysiwyg">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <?php
});
