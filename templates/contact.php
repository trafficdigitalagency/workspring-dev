<?php
/**
 * Template Name: Contact
 */

SOP_loop(function () {
    ?>
    <div class="page-header"  <?php echo SOP_backgroundImage(get_post_thumbnail_id(), 'page-header'); ?>>
        <h1 class="header-title"><?php the_title(); ?></h1>
    </div>
    <div class="container">
        <div class="content-container">
            <div class="row">
                <div class="col-sm-6 wysiwyg">
                    <?php the_content(); ?>
                </div>

                <div class="col-sm-6">
                    <h3 class="large-heading"><?php the_title(); ?></h3>

                    <div class="contact-form">
                        <?php SOP_enqueueForm(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
});