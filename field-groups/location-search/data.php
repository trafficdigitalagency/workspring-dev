<?php

if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_location-search',
    'title' => 'Location Search',
    'fields' => array (
      array (
        'key' => 'field_53d01fd1c738e',
        'label' => 'Location Search',
        'name' => 'location_search',
        'type' => 'true_false',
        'instructions' => 'Activate Location Search',
        'message' => '',
        'default_value' => 0,
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'acf-options',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array (
      ),
    ),
    'menu_order' => 0,
  ));
}
