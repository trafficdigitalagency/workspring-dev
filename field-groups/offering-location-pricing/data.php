<?php
$group = array(
    'id' => '538dd81a7b697',
    'title' => 'Offering Location Pricing',
    'fields' => array(
        array(
            'key' => 'field_538cd8d9ctas',
            'label' => 'CTAs',
            'name' => 'ctas',
            '_name' => 'ctas',
            'type' => 'repeater',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-ctas',
            'class' => 'repeater',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'sub_fields' => array(
                array(
                    'key' => 'field_538cd90coffcta',
                    'label' => 'CTA Label',
                    'name' => 'label',
                    '_name' => 'label',
                    'type' => 'text',
                    'order_no' => 0,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-label',
                    'class' => 'text',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_538cd90ctaparams',
                    'label' => 'Parameters',
                    'name' => 'parameters',
                    '_name' => 'parameters',
                    'type' => 'repeater',
                    'order_no' => 0,
                    'instructions' => 'NOTE: Be sure to update the respective form field values for the booking form',
                    'required' => 0,
                    'id' => 'acf-field-parameters',
                    'class' => 'repeater',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'sub_fields' => array(
                        array(
                            'key' => 'field_538key',
                            'label' => 'Key',
                            'name' => 'key',
                            '_name' => 'key',
                            'type' => 'text',
                            'order_no' => 0,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-key',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    0 => array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'none',
                            'maxlength' => ''
                        ),
                        array(
                            'key' => 'field_538value',
                            'label' => 'Value',
                            'name' => 'value',
                            '_name' => 'value',
                            'type' => 'text',
                            'order_no' => 0,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-value',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    0 => array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'none',
                            'maxlength' => ''
                        ),
                    ),
                    'row_min' => 0,
                    'row_limit' => '',
                    'layout' => 'row',
                    'button_label' => 'Add Param',
                ),
            ),
            'row_min' => 1,
            'row_limit' => '',
            'layout' => 'row',
            'button_label' => 'Add CTA',
        ),
        array(
            'key' => 'field_538cd8d96d896',
            'label' => 'Location Pricing',
            'name' => 'location_pricing',
            '_name' => 'location_pricing',
            'type' => 'repeater',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-location_pricing',
            'class' => 'repeater',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'sub_fields' => array(
                0 => array(
                    'key' => 'field_538cd9a946dd7',
                    'label' => 'Location',
                    'name' => 'location',
                    '_name' => 'location',
                    'type' => 'post_object',
                    'order_no' => 0,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-location',
                    'class' => 'post_object',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'post_type' => array(
                        0 => 'location'
                    ),
                    'taxonomy' => array(
                        0 => 'all'
                    ),
                    'allow_null' => 0,
                    'multiple' => 0
                ),
                1 => array(
                    'key' => 'field_538cd8ee6d897',
                    'label' => 'Variables',
                    'name' => 'variables',
                    '_name' => 'variables',
                    'type' => 'repeater',
                    'order_no' => 1,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-variables',
                    'class' => 'repeater',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '=='
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'sub_fields' => array(
                        0 => array(
                            'key' => 'field_538cd90c6d898',
                            'label' => 'Type',
                            'name' => 'type',
                            '_name' => 'type',
                            'type' => 'text',
                            'order_no' => 0,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-type',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    0 => array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => ''
                        ),
                        1 => array(
                            'key' => 'field_538cd9346d899',
                            'label' => 'Options',
                            'name' => 'options',
                            '_name' => 'options',
                            'type' => 'repeater',
                            'order_no' => 1,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-options',
                            'class' => 'repeater',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    0 => array(
                                        'field' => 'null',
                                        'operator' => '=='
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'sub_fields' => array(
                                0 => array(
                                    'key' => 'field_538cd9476d89a',
                                    'label' => 'Label',
                                    'name' => 'label',
                                    '_name' => 'label',
                                    'type' => 'text',
                                    'order_no' => 0,
                                    'instructions' => '',
                                    'required' => 0,
                                    'id' => 'acf-field-label',
                                    'class' => 'text',
                                    'conditional_logic' => array(
                                        'status' => 0,
                                        'rules' => array(
                                            0 => array(
                                                'field' => 'null',
                                                'operator' => '==',
                                                'value' => ''
                                            )
                                        ),
                                        'allorany' => 'all'
                                    ),
                                    'column_width' => '',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'formatting' => 'html',
                                    'maxlength' => ''
                                ),
                                1 => array(
                                    'key' => 'field_538cd94c6d89b',
                                    'label' => 'Multiplier',
                                    'name' => 'multiplier',
                                    '_name' => 'multiplier',
                                    'type' => 'number',
                                    'order_no' => 1,
                                    'instructions' => '',
                                    'required' => 0,
                                    'id' => 'acf-field-multiplier',
                                    'class' => 'number',
                                    'conditional_logic' => array(
                                        'status' => 0,
                                        'rules' => array(
                                            0 => array(
                                                'field' => 'null',
                                                'operator' => '=='
                                            )
                                        ),
                                        'allorany' => 'all'
                                    ),
                                    'column_width' => '',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'min' => 0,
                                    'max' => '',
                                    'step' => ''
                                ),
                                2 => array(
                                    'key' => 'field_538cd9556d89c',
                                    'label' => 'Unit',
                                    'name' => 'unit',
                                    '_name' => 'unit',
                                    'type' => 'text',
                                    'order_no' => 2,
                                    'instructions' => 'Optional unit appended to calculated price',
                                    'required' => 0,
                                    'id' => 'acf-field-unit',
                                    'class' => 'text',
                                    'conditional_logic' => array(
                                        'status' => 0,
                                        'rules' => array(
                                            0 => array(
                                                'field' => 'null',
                                                'operator' => '==',
                                                'value' => ''
                                            )
                                        ),
                                        'allorany' => 'all'
                                    ),
                                    'column_width' => '',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'formatting' => 'html',
                                    'maxlength' => ''
                                )
                            ),
                            'row_min' => 1,
                            'row_limit' => '',
                            'layout' => 'row',
                            'button_label' => 'Add Option'
                        )
                    ),
                    'row_min' => 1,
                    'row_limit' => '',
                    'layout' => 'row',
                    'button_label' => 'Add Variable'
                )
            ),
            'row_min' => 1,
            'row_limit' => '',
            'layout' => 'row',
            'button_label' => 'Add Offering',
            'field_group' => 1758
        )
    ),
    'location' => array(
        0 => array(
            0 => array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'offering',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
