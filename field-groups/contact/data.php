<?php
$group = array (
  'id' => '537e662b8bb49',
  'title' => 'Contact',
  'fields' =>
  array (
    0 =>
    array (
      'key' => 'field_537e64f55c107',
      'label' => 'Form',
      'name' => 'form',
      '_name' => 'form',
      'type' => 'gravity_forms_field',
      'order_no' => 0,
      'instructions' => '',
      'required' => 1,
      'id' => 'acf-field-form',
      'class' => 'gravity_forms_field',
      'conditional_logic' =>
      array (
        'status' => 0,
        'rules' =>
        array (
          0 =>
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'field_group' => 360,
    ),
  ),
  'location' =>
  array (
    0 =>
    array (
      array (
        'param' => 'page_template',
        'operator' => '==',
        'value' => 'templates/contact.php',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
    array(
      array (
        'param' => 'page_template',
        'operator' => '==',
        'value' => 'templates/book.php',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' =>
  array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' =>
    array (
    ),
  ),
  'menu_order' => 0,
);
