<?php
$group = array(
    'id' => '537e662b53c34',
    'title' => 'Location Offerings',
    'fields' => array(
        array(
            'key' => 'field_537e4e02626c3',
            'label' => 'Offerings',
            'name' => 'offerings',
            '_name' => 'offerings',
            'type' => 'repeater',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-offerings',
            'class' => 'repeater',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'sub_fields' => array(
                array(
                    'key' => 'field_537e4e3b626c6',
                    'label' => 'Type',
                    'name' => 'type',
                    '_name' => 'type',
                    'type' => 'post_object',
                    'order_no' => 0,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-type',
                    'class' => 'post_object',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'post_type' => array(
                        0 => 'offering'
                    ),
                    'taxonomy' => array(
                        0 => 'all'
                    ),
                    'allow_null' => 0,
                    'multiple' => 0
                ),
                array(
                    'key' => 'field_537e4e10626c4',
                    'label' => 'Slider',
                    'name' => 'slider',
                    '_name' => 'slider',
                    'type' => 'repeater',
                    'order_no' => 1,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-slider',
                    'class' => 'repeater',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_537e4e16626c5',
                            'label' => 'Image',
                            'name' => 'image',
                            '_name' => 'image',
                            'type' => 'image',
                            'order_no' => 0,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-image',
                            'class' => 'image',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'save_format' => 'id',
                            'preview_size' => 'thumbnail',
                            'library' => 'all'
                        ),
                        array(
                            'key' => 'field_537e4f3702e3f',
                            'label' => 'Caption',
                            'name' => 'caption',
                            '_name' => 'caption',
                            'type' => 'text',
                            'order_no' => 1,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-caption',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => ''
                        )
                    ),
                    'row_min' => '',
                    'row_limit' => '',
                    'layout' => 'row',
                    'button_label' => 'Add Slide'
                ),
                array(
                    'key' => 'field_537e4f4d02e40',
                    'label' => 'Description',
                    'name' => 'description',
                    '_name' => 'description',
                    'type' => 'textarea',
                    'order_no' => 2,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-description',
                    'class' => 'textarea',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'formatting' => 'html'
                ),

                array(
                    'key' => 'field_af6d0cf85aa2a8',
                    'label' => 'Addition Info',
                    'name' => 'additional_info',
                    '_name' => 'additional_info',
                    'type' => 'wysiwyg',
                    'order_no' => 2,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-additional-info',
                    'class' => 'wysiwyg',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'toolbar' => 'basic',
                    'media_upload' => 'no',
                ),
                array(
                    'key' => 'field_537e4f7002e41',
                    'label' => 'Space Details',
                    'name' => 'space_details',
                    '_name' => 'space_details',
                    'type' => 'repeater',
                    'order_no' => 3,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-space_details',
                    'class' => 'repeater',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            array(
                                'field' => 'null',
                                'operator' => '=='
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_537e4f7d02e42',
                            'label' => 'Space Name',
                            'name' => 'space_name',
                            '_name' => 'space_name',
                            'type' => 'text',
                            'order_no' => 0,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-space_name',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => ''
                        ),
                        array(
                            'key' => 'field_537e4f8402e43',
                            'label' => 'Capacity',
                            'name' => 'capacity',
                            '_name' => 'capacity',
                            'type' => 'text',
                            'order_no' => 1,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-capacity',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => ''
                        ),
                        array(
                            'key' => 'field_537e4f8802e44',
                            'label' => 'Dimensions',
                            'name' => 'dimensions',
                            '_name' => 'dimensions',
                            'type' => 'text',
                            'order_no' => 2,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-dimensions',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => ''
                        ),
                        array(
                            'key' => 'field_537e4f8d02e45',
                            'label' => 'Tools and Technology',
                            'name' => 'tools_and_technology',
                            '_name' => 'tools_and_technology',
                            'type' => 'text',
                            'order_no' => 3,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-tools_and_technology',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => ''
                        ),
                        array(
                            'key' => 'field_537e4f9602e46',
                            'label' => 'Floorplan',
                            'name' => 'floorplan',
                            '_name' => 'floorplan',
                            'type' => 'file',
                            'order_no' => 4,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-floorplan',
                            'class' => 'file',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    array(
                                        'field' => 'null',
                                        'operator' => '=='
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'save_format' => 'url',
                            'library' => 'all'
                        ),
                        array(
                            'key' => 'field_537e4fc66ceea',
                            'label' => 'Virtual Tour URL',
                            'name' => 'virtual_tour_url',
                            '_name' => 'virtual_tour_url',
                            'type' => 'text',
                            'order_no' => 5,
                            'instructions' => '',
                            'required' => 0,
                            'id' => 'acf-field-virtual_tour_url',
                            'class' => 'text',
                            'conditional_logic' => array(
                                'status' => 0,
                                'rules' => array(
                                    array(
                                        'field' => 'null',
                                        'operator' => '==',
                                        'value' => ''
                                    )
                                ),
                                'allorany' => 'all'
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'none',
                            'maxlength' => ''
                        )
                    ),
                    'row_min' => '',
                    'row_limit' => '',
                    'layout' => 'row',
                    'button_label' => 'Add Row'
                )
            ),
            'row_min' => '',
            'row_limit' => '',
            'layout' => 'row',
            'button_label' => 'Add Offering',
            'field_group' => 291
        )
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'location',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
