<?php
$group = array(
    'id' => '5388f46328689',
    'title' => 'Post',
    'fields' => array(
        array(
            'key' => 'field_538e4afeatvideo',
            'label' => 'Featured Video',
            'name' => 'featured_video',
            '_name' => 'featured_video',
            'type' => 'text',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-featured_video',
            'class' => 'text',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'message' => '',
            'formatting' => 'none'
        ),
        array(
            'key' => 'field_5388d0e3634fb',
            'label' => 'Author',
            'name' => 'author',
            '_name' => 'author',
            'type' => 'post_object',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-author',
            'class' => 'post_object',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'post_type' => array(
                0 => 'person'
            ),
            'taxonomy' => array(
                0 => 'all'
            ),
            'allow_null' => 1,
            'multiple' => 0,
            'field_group' => 251
        )
    ),
    'location' => array(
        0 => array(
            0 => array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'post',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
