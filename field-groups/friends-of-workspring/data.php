<?php
$group = array(
    'id' => '537e662b85a45',
    'title' => 'Friends of Workspring',
    'fields' => array(
        0 => array(
            'key' => 'field_537e63d374b73',
            'label' => 'Related Pages',
            'name' => 'articles',
            '_name' => 'articles',
            'type' => 'relationship',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-friends',
            'class' => 'relationship',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'return_format' => 'id',
            'post_type' => array(
                'page',
                'post'
            ),
            'taxonomy' => array(
                0 => 'all'
            ),
            'filters' => array(
                0 => 'search'
            ),
            'result_elements' => array(
                0 => 'featured_image',
                1 => 'post_type',
                2 => 'post_title'
            ),
            'max' => '',
            'field_group' => 357
        ),
        array(
            'key' => 'field_friendheading',
            'label' => 'Heading',
            'name' => 'heading',
            '_name' => 'heading',
            'type' => 'text',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-heading',
            'class' => 'text',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(

                    array(
                        'field' => 'null',
                        'operator' => '==',
                        'value' => ''
                    )
                ),
                'allorany' => 'all'
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 232
        ),
    ),
    'location' => array(
        0 => array(
            0 => array(
                'param' => 'page_template',
                'operator' => '==',
                'value' => 'templates/friends.php',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'no_box',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
