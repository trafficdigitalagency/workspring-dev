<?php 
$group = array (
  'id' => '537e662b6e5a0',
  'title' => 'Location Find Us',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_537e5172b5b8e',
      'label' => 'Directions',
      'name' => 'directions',
      '_name' => 'directions',
      'type' => 'wp_wysiwyg',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-directions',
      'class' => 'wp_wysiwyg',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'teeny' => 0,
      'media_buttons' => 1,
      'dfw' => 1,
      'field_group' => 295,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'location',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' => 
    array (
    ),
  ),
  'menu_order' => 0,
);