<?php
$group = array(
    'id' => '538e3fb7391fb',
    'title' => 'Pages',
    'fields' => array(
        array(
            'key' => 'field_538e2629b4106',
            'label' => 'Booking Page',
            'name' => 'booking_page',
            '_name' => 'booking_page',
            'type' => 'page_link',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-booking_page',
            'class' => 'page_link',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'post_type' => array(
                0 => 'page'
            ),
            'allow_null' => 0,
            'multiple' => 0,
            'field_group' => 1760
        ),
        array(
            'key' => 'field_538e2emailestpage',
            'label' => 'Estimate Page',
            'name' => 'estimate_page',
            '_name' => 'estimate_page',
            'type' => 'page_link',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-estimate_page',
            'class' => 'page_link',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'post_type' => array(
                0 => 'page'
            ),
            'allow_null' => 0,
            'multiple' => 0,
            'field_group' => 1760
        ),
        array(
            'key' => 'field_538e2contactpage',
            'label' => 'Contact Page',
            'name' => 'contact_page',
            '_name' => 'contact_page',
            'type' => 'page_link',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-contact_page',
            'class' => 'page_link',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'post_type' => array(
                0 => 'page'
            ),
            'allow_null' => 0,
            'multiple' => 0,
            'field_group' => 1760
        ),
        array(
            'key' => 'field_538e2peoplesubcopy',
            'label' => 'People Page Subcopy',
            'name' => 'people_subcopy',
            '_name' => 'people_subcopy',
            'type' => 'textarea',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-people_subcopy',
            'class' => 'textarea',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
        ),
    ),
    'location' => array(
        0 => array(
            0 => array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'no_box',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
