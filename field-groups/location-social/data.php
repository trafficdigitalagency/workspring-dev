<?php
$group = array(
    'id' => '537e662b4b593',
    'title' => 'Location Social',
    'fields' => array(
        0 => array(
            'key' => 'field_537e4d4d0b2c2',
            'label' => 'Social',
            'name' => 'social',
            '_name' => 'social',
            'type' => 'repeater',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-social',
            'class' => 'repeater',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'sub_fields' => array(
                0 => array(
                    'key' => 'field_537e4d560b2c3',
                    'label' => 'Type',
                    'name' => 'type',
                    '_name' => 'type',
                    'type' => 'select',
                    'order_no' => 0,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-type',
                    'class' => 'select',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '=='
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'choices' => array(
                        'facebook' => 'Facebook',
                        'twitter' => 'Twitter',
                        'linkedin' => 'LinkedIn',
                        'youtube' => 'Youtube',
                        'googleplus' => 'GooglePlus',
                        'yelp' => 'Yelp'
                    ),
                    'default_value' => '',
                    'allow_null' => 0,
                    'multiple' => 0
                ),
                1 => array(
                    'key' => 'field_537e4d7f0b2c4',
                    'label' => 'URL',
                    'name' => 'url',
                    '_name' => 'url',
                    'type' => 'text',
                    'order_no' => 1,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-url',
                    'class' => 'text',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'field_537e4d560b2c3',
                                'operator' => '==',
                                'value' => 'facebook'
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => ''
                )
            ),
            'row_min' => '',
            'row_limit' => '',
            'layout' => 'table',
            'button_label' => 'Add Row',
            'field_group' => 290
        )
    ),
    'location' => array(
        0 => array(
            0 => array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'location',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
