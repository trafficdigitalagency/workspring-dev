<?php
$group = array(
    'id' => 'acf_front-page',
    'title' => 'Front Page',
    'fields' => array(
        array(
            'key' => 'field_5387b15c7c1f6',
            'label' => 'Top Slider',
            'name' => 'top_slider',
            'type' => 'repeater',
            'sub_fields' => array(
                array(
                    'key' => 'field_5387b1687c1f7',
                    'label' => 'Heading',
                    'name' => 'heading',
                    'type' => 'textarea',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'br',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_5387b16e7c1f8',
                    'label' => 'Subheading',
                    'name' => 'subheading',
                    'type' => 'text',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_5387b1757c1f9',
                    'label' => 'CTA',
                    'name' => 'cta',
                    'type' => 'text',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_5387b17b7c1fa',
                    'label' => 'URL',
                    'name' => 'url',
                    'type' => 'text',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_538cftop36',
                    'label' => 'link',
                    'name' => 'link',
                    '_name' => 'link',
                    'type' => 'page_link',
                    'order_no' => 0,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-link',
                    'class' => 'page_link',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '=='
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'post_type' => array(
                        0 => 'post',
                        1 => 'page',
                        2 => 'location',
                        3 => 'offering',
                        4 => 'person'
                    ),
                    'allow_null' => 1,
                    'multiple' => 0
                ),
                array(
                    'key' => 'field_5387b18c7c375',
                    'label' => 'Is Video',
                    'name' => 'video',
                    'type' => 'true_false',
                    'column_width' => '',
                    'message' => '',
                    'default_value' => 0
                ),
                array(
                    'key' => 'field_5387bopenmodal',
                    'label' => 'Open in Modal',
                    'name' => 'modal',
                    'type' => 'true_false',
                    'column_width' => '',
                    'message' => '',
                    'default_value' => 0
                ),
                array(
                    'key' => 'field_5387b1977c376',
                    'label' => 'Image',
                    'name' => 'image',
                    'type' => 'image',
                    'column_width' => '',
                    'save_format' => 'id',
                    'preview_size' => 'thumbnail',
                    'library' => 'all'
                ),
                array(
                    'key' => 'field_5387b19d7c377',
                    'label' => 'Caption',
                    'name' => 'caption',
                    'type' => 'text',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => ''
                )
            ),
            'row_min' => '',
            'row_limit' => '',
            'layout' => 'row',
            'button_label' => 'Add Slide'
        ),
        array (
          'key' => 'field_55e0d229cc9c5',
          'label' => 'Under Slider Callout',
          'name' => 'under_slider_callout',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'none',
          'maxlength' => 40,
        ),
        array (
          'key' => 'field_55e0d243cc9c6',
          'label' => 'Under Slider Callout CTA',
          'name' => 'under_slider_callout_cta',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'none',
          'maxlength' => 20,
        ),
        array (
          'key' => 'field_55e0d276cc9c7',
          'label' => 'Under Slider Callout Link Type',
          'name' => 'under_slider_callout_link_type',
          'type' => 'radio',
          'choices' => array (
            'internal' => 'Internal Page',
            'custom' => 'Custom Link',
          ),
          'other_choice' => 0,
          'save_other_choice' => 0,
          'default_value' => '',
          'layout' => 'horizontal',
        ),
        array (
          'key' => 'field_55e0d2b4e588c',
          'label' => 'Under Slider Callout Link',
          'name' => 'under_slider_callout_link',
          'type' => 'page_link',
          'conditional_logic' => array (
            'status' => 1,
            'rules' => array (
              array (
                'field' => 'field_55e0d276cc9c7',
                'operator' => '==',
                'value' => 'internal',
              ),
            ),
            'allorany' => 'all',
          ),
          'post_type' => array (
            0 => 'post',
            1 => 'page',
            2 => 'attachment',
            3 => 'location',
            4 => 'offering',
            5 => 'person',
          ),
          'allow_null' => 0,
          'multiple' => 0,
        ),
        array (
          'key' => 'field_55e0d2c6e588d',
          'label' => 'Under Slider Callout Custom Link',
          'name' => 'under_slider_callout_custom_link',
          'type' => 'text',
          'conditional_logic' => array (
            'status' => 1,
            'rules' => array (
              array (
                'field' => 'field_55e0d276cc9c7',
                'operator' => '==',
                'value' => 'custom',
              ),
            ),
            'allorany' => 'all',
          ),
          'default_value' => '',
          'placeholder' => 'http://workspring.com',
          'prepend' => '',
          'append' => '',
          'formatting' => 'none',
          'maxlength' => '',
        ),
        array(
            'key' => 'field_5387b6aad81d9',
            'label' => 'News Slider',
            'name' => 'news_slider',
            'type' => 'repeater',
            'sub_fields' => array(
                array(
                    'key' => 'field_5387b6aad81da',
                    'label' => 'Heading',
                    'name' => 'heading',
                    'type' => 'text',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_5387b6aad81db',
                    'label' => 'Subheading',
                    'name' => 'subheading',
                    'type' => 'text',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_5387b6aad81dc',
                    'label' => 'CTA',
                    'name' => 'cta',
                    'type' => 'text',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_5387b6aad81dd',
                    'label' => 'URL',
                    'name' => 'url',
                    'type' => 'text',
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => ''
                ),
                array(
                    'key' => 'field_538cf2bd92736',
                    'label' => 'link',
                    'name' => 'link',
                    '_name' => 'link',
                    'type' => 'page_link',
                    'order_no' => 0,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-link',
                    'class' => 'page_link',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '=='
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'post_type' => array(
                        0 => 'post',
                        1 => 'page',
                        2 => 'location',
                        3 => 'offering',
                        4 => 'person'
                    ),
                    'allow_null' => 1,
                    'multiple' => 0
                ),
                array(
                    'key' => 'field_5387b6aad81de',
                    'label' => 'Is Video',
                    'name' => 'video',
                    'type' => 'true_false',
                    'column_width' => '',
                    'message' => '',
                    'default_value' => 0
                ),
                array(
                    'key' => 'field_5387aopenmodal',
                    'label' => 'Open in Modal',
                    'name' => 'modal',
                    'type' => 'true_false',
                    'column_width' => '',
                    'message' => '',
                    'default_value' => 0
                ),
                array(
                    'key' => 'field_5387b6aad81df',
                    'label' => 'Image',
                    'name' => 'image',
                    'type' => 'image',
                    'column_width' => '',
                    'save_format' => 'id',
                    'preview_size' => 'thumbnail',
                    'library' => 'all'
                )
            ),
            'row_min' => '',
            'row_limit' => '',
            'layout' => 'row',
            'button_label' => 'Add Slide'
        )
    ),
    'location' => array(
        array(
            array(
                'param' => 'page_type',
                'operator' => '==',
                'value' => 'front_page',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
