<?php
$group = array(
    'id' => '538cf51dead32',
    'title' => 'Offering Booking Info',
    'fields' => array(
        0 => array(
            'key' => 'field_538cf4d6c1f1d',
            'label' => 'Header',
            'name' => 'header',
            '_name' => 'header',
            'type' => 'text',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-header',
            'class' => 'text',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '==',
                        'value' => ''
                    )
                ),
                'allorany' => 'all'
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 1752
        ),
        1 => array(
            'key' => 'field_538cf4df66bfc',
            'label' => 'Subcopy',
            'name' => 'subcopy',
            '_name' => 'subcopy',
            'type' => 'wysiwyg',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-subcopy',
            'class' => 'wysiwyg',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '==',
                        'value' => ''
                    )
                ),
                'allorany' => 'all'
            ),
            'default_value' => '',
            'toolbar' => 'basic',
            'media_upload' => 'no',
            'field_group' => 1752
        ),
        2 => array(
            'key' => 'field_538cf5036acd0',
            'label' => 'Additional Info',
            'name' => 'additional_info',
            '_name' => 'additional_info',
            'type' => 'text',
            'order_no' => 2,
            'instructions' => 'Shown on hovering over information icon',
            'required' => 0,
            'id' => 'acf-field-additional_info',
            'class' => 'text',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 1752
        ),
        3 => array(
            'key' => 'field_538cf503emailest',
            'label' => 'Email Estimate Text',
            'name' => 'email_estimate',
            '_name' => 'email_estimate',
            'type' => 'textarea',
            'order_no' => 3,
            'instructions' => 'Shown on email estimate',
            'required' => 0,
            'id' => 'acf-field-email_estimate',
            'class' => 'textarea',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'none',
            'maxlength' => '',
            'field_group' => 1752
        )
    ),
    'location' => array(
        0 => array(
            0 => array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'offering',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
