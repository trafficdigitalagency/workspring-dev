<?php
$group = array (
  'id' => '538cefae099e0',
  'title' => 'Location Event',
  'fields' =>
  array (
    0 =>
    array (
      'key' => 'field_538cef5c8260c',
      'label' => 'Title',
      'name' => 'event_title',
      '_name' => 'event_title',
      'type' => 'text',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-event_title',
      'class' => 'text',
      'conditional_logic' =>
      array (
        'status' => 0,
        'rules' =>
        array (
          0 =>
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
      'field_group' => 1750,
    ),
    1 =>
    array (
      'key' => 'field_538cef628260d',
      'label' => 'Excerpt',
      'name' => 'event_excerpt',
      '_name' => 'event_excerpt',
      'type' => 'textarea',
      'order_no' => 1,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-event_excerpt',
      'class' => 'textarea',
      'conditional_logic' =>
      array (
        'status' => 0,
        'rules' =>
        array (
          0 =>
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'placeholder' => '',
      'maxlength' => '',
      'rows' => '',
      'formatting' => 'br',
      'field_group' => 1750,
    ),
    2 =>
    array (
      'key' => 'field_538cef6a8260e',
      'label' => 'URL',
      'name' => 'event_url',
      '_name' => 'event_url',
      'type' => 'text',
      'order_no' => 2,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-event_url',
      'class' => 'text',
      'conditional_logic' =>
      array (
        'status' => 0,
        'rules' =>
        array (
          0 =>
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
      'field_group' => 1750,
    ),
    3 =>
    array (
      'key' => 'field_538cef778260f',
      'label' => 'Internal Link',
      'name' => 'event_internal_link',
      '_name' => 'event_internal_link',
      'type' => 'page_link',
      'order_no' => 3,
      'instructions' => 'Overrides URL',
      'required' => 0,
      'id' => 'acf-field-event_internal_link',
      'class' => 'page_link',
      'conditional_logic' =>
      array (
        'status' => 0,
        'rules' =>
        array (
          0 =>
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'post_type' =>
      array (
        0 => 'post',
        1 => 'page',
      ),
      'allow_null' => 1,
      'multiple' => 0,
      'field_group' => 1750,
    ),
  ),
  'location' =>
  array (
    0 =>
    array (
      0 =>
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'location',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' =>
  array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' =>
    array (
    ),
  ),
  'menu_order' => 0,
);
