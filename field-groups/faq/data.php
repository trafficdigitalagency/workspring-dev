<?php 
$group = array (
  'id' => '537e662b7bdf2',
  'title' => 'FAQ',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_537e5c6ed6f9d',
      'label' => 'Sections',
      'name' => 'sections',
      '_name' => 'sections',
      'type' => 'repeater',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-sections',
      'class' => 'repeater',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'sub_fields' => 
      array (
        0 => 
        array (
          'key' => 'field_537e5d0a1654a',
          'label' => 'Title',
          'name' => 'title',
          '_name' => 'title',
          'type' => 'text',
          'order_no' => 0,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-title',
          'class' => 'text',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        1 => 
        array (
          'key' => 'field_537e5c8fd6f9f',
          'label' => 'Questions',
          'name' => 'questions',
          '_name' => 'questions',
          'type' => 'repeater',
          'order_no' => 1,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-questions',
          'class' => 'repeater',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'sub_fields' => 
          array (
            0 => 
            array (
              'key' => 'field_537e5ca9d6fa0',
              'label' => 'Question',
              'name' => 'question',
              '_name' => 'question',
              'type' => 'text',
              'order_no' => 0,
              'instructions' => '',
              'required' => 0,
              'id' => 'acf-field-question',
              'class' => 'text',
              'conditional_logic' => 
              array (
                'status' => 0,
                'rules' => 
                array (
                  0 => 
                  array (
                    'field' => 'null',
                    'operator' => '==',
                    'value' => '',
                  ),
                ),
                'allorany' => 'all',
              ),
              'column_width' => '',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            1 => 
            array (
              'key' => 'field_537e5caed6fa1',
              'label' => 'Answer',
              'name' => 'answer',
              '_name' => 'answer',
              'type' => 'wysiwyg',
              'order_no' => 1,
              'instructions' => '',
              'required' => 0,
              'id' => 'acf-field-answer',
              'class' => 'wysiwyg',
              'conditional_logic' => 
              array (
                'status' => 0,
                'rules' => 
                array (
                  0 => 
                  array (
                    'field' => 'null',
                    'operator' => '==',
                    'value' => '',
                  ),
                ),
                'allorany' => 'all',
              ),
              'column_width' => '',
              'default_value' => '',
              'toolbar' => 'basic',
              'media_upload' => 'no',
            ),
          ),
          'row_min' => 1,
          'row_limit' => '',
          'layout' => 'row',
          'button_label' => 'Add Question',
        ),
      ),
      'row_min' => 1,
      'row_limit' => '',
      'layout' => 'row',
      'button_label' => 'Add Section',
      'field_group' => 352,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'page_template',
        'operator' => '==',
        'value' => 'templates/faq.php',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
    ),
  ),
  'menu_order' => 0,
);