<?php 
$group = array (
  'id' => '537e662b646ff',
  'title' => 'Location Local Guide',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_537e4ff3c0015',
      'label' => 'Categories',
      'name' => 'categories',
      '_name' => 'categories',
      'type' => 'repeater',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-categories',
      'class' => 'repeater',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'sub_fields' => 
      array (
        0 => 
        array (
          'key' => 'field_537e5007c0016',
          'label' => 'Name',
          'name' => 'name',
          '_name' => 'name',
          'type' => 'text',
          'order_no' => 0,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-name',
          'class' => 'text',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        1 => 
        array (
          'key' => 'field_537e500cc0017',
          'label' => 'Links',
          'name' => 'links',
          '_name' => 'links',
          'type' => 'repeater',
          'order_no' => 1,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-links',
          'class' => 'repeater',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'sub_fields' => 
          array (
            0 => 
            array (
              'key' => 'field_537e5060c0018',
              'label' => 'Label',
              'name' => 'label',
              '_name' => 'label',
              'type' => 'text',
              'order_no' => 0,
              'instructions' => '',
              'required' => 0,
              'id' => 'acf-field-label',
              'class' => 'text',
              'conditional_logic' => 
              array (
                'status' => 0,
                'rules' => 
                array (
                  0 => 
                  array (
                    'field' => 'null',
                    'operator' => '==',
                    'value' => '',
                  ),
                ),
                'allorany' => 'all',
              ),
              'column_width' => '',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            1 => 
            array (
              'key' => 'field_537e5064c0019',
              'label' => 'URL',
              'name' => 'url',
              '_name' => 'url',
              'type' => 'text',
              'order_no' => 1,
              'instructions' => '',
              'required' => 0,
              'id' => 'acf-field-url',
              'class' => 'text',
              'conditional_logic' => 
              array (
                'status' => 0,
                'rules' => 
                array (
                  0 => 
                  array (
                    'field' => 'null',
                    'operator' => '==',
                    'value' => '',
                  ),
                ),
                'allorany' => 'all',
              ),
              'column_width' => '',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'none',
              'maxlength' => '',
            ),
          ),
          'row_min' => '',
          'row_limit' => '',
          'layout' => 'table',
          'button_label' => 'Add Link',
        ),
      ),
      'row_min' => '',
      'row_limit' => '',
      'layout' => 'row',
      'button_label' => 'Add Category',
      'field_group' => 292,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'location',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' => 
    array (
    ),
  ),
  'menu_order' => 0,
);