<?php
$group = array(
    'id' => '53889448ce74f',
    'title' => 'People',
    'fields' => array(
        0 => array(
            'key' => 'field_5387ba4f9f248',
            'label' => 'Job Title',
            'name' => 'position',
            '_name' => 'position',
            'type' => 'text',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-position',
            'class' => 'text',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 220
        ),
        array(
            'key' => 'field_537e8e8c6755f',
            'label' => 'Header Image',
            'name' => 'header_image',
            '_name' => 'header_image',
            'type' => 'image',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-header_image',
            'class' => 'image',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    array(
                        'field' => 'null',
                        'operator' => '=='
                    )
                ),
                'allorany' => 'all'
            ),
            'save_format' => 'id',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'field_group' => 220
        )
    ),
    'location' => array(
        0 => array(
            0 => array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'person',
                'order_no' => 0,
                'group_no' => 0
            )
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array()
    ),
    'menu_order' => 0
);
