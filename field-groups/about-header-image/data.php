<?php
$group = array (
  'id' => '538e3fb73d824',
  'title' => 'About Header Image',
  'fields' =>
  array (
    0 =>
    array (
      'key' => 'field_538e3f801b461',
      'label' => 'Image',
      'name' => 'about_header_image',
      '_name' => 'about_header_image',
      'type' => 'image',
      'order_no' => 0,
      'instructions' => 'Appears in header of all About Section pages',
      'required' => 0,
      'id' => 'acf-field-image',
      'class' => 'image',
      'conditional_logic' =>
      array (
        'status' => 0,
        'rules' =>
        array (
          0 =>
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'save_format' => 'id',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'field_group' => 1761,
    ),
  ),
  'location' =>
  array (
    0 =>
    array (
      0 =>
      array (
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'acf-options',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' =>
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' =>
    array (
    ),
  ),
  'menu_order' => 0,
);
