<?php
$group = array (
    'id' => '537e662b32df4',
    'title' => 'Offerings',
    'fields' =>
    array (
        array (
            'key' => 'field_537e0931924asf',
            'label' => 'Overview',
            'name' => 'overview',
            '_name' => 'overview',
            'type' => 'textarea',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-overview',
            'class' => 'textarea',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (
                    0 =>
                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'formatting' => 'br',
            'field_group' => 9,
        ),
        array (
            'key' => 'field_537e09319247f',
            'label' => 'Amenities',
            'name' => 'amenities',
            '_name' => 'amenities',
            'type' => 'repeater',
            'order_no' => 0,
            'instructions' => 'One amenity per line',
            'required' => 0,
            'id' => 'acf-field-amenities',
            'class' => 'repeater',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (
                    0 =>
                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'sub_fields' => array (
                array (
                    'key' => 'field_amenityinfo',
                    'label' => 'Info',
                    'name' => 'info',
                    '_name' => 'type',
                    'type' => 'text',
                    'order_no' => 0,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-info',
                    'class' => 'text',
                    'conditional_logic' =>
                    array (
                        'status' => 0,
                        'rules' => array (
                            array (
                                'field' => 'null',
                                'operator' => '==',
                                'value' => '',
                            ),
                        ),
                        'allorany' => 'all',
                    ),
                    'column_width' => '',
                    'allow_null' => 0,
                    'multiple' => 0,
                ),
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'formatting' => 'none',
            'field_group' => 9,
        ),
        array (
            'key' => 'field_537e0a87f700f',
            'label' => 'Amenities Callout',
            'name' => 'amenities_callout',
            '_name' => 'amenities_callout',
            'type' => 'wysiwyg',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-amenities_callout',
            'class' => 'wysiwyg',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (
                    0 =>
                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'toolbar' => 'full',
            'media_upload' => 'yes',
            'field_group' => 9,
        ),
        array (
            'key' => 'field_537e53ba3968f',
            'label' => 'Starting Cost',
            'name' => 'starting_cost',
            '_name' => 'starting_cost',
            'type' => 'text',
            'order_no' => 2,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-starting_cost',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (
                    0 =>
                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => 'Starts at $/day',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 9,
        ),
    ),
    'location' =>
    array (
        0 =>
        array (
            0 =>
            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'offering',
                'order_no' => 0,
                'group_no' => 0,
            ),
        ),
    ),
    'options' =>
    array (
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' =>
        array (
        ),
    ),
    'menu_order' => 0,
);
