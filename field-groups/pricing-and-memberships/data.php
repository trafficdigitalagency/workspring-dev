<?php
$group = array (
  'id' => '53889448d0b88',
  'title' => 'Pricing and Memberships',
  'fields' =>
  array (
    0 =>
    array (
      'key' => 'field_5387bb4bfdcd8',
      'label' => 'Promotions',
      'name' => 'articles',
      '_name' => 'articles',
      'type' => 'relationship',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-promotions',
      'class' => 'relationship',
      'conditional_logic' =>
      array (
        'status' => 0,
        'rules' =>
        array (
          0 =>
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'id',
      'post_type' =>
      array (
        0 => 'post',
        1 => 'page',
        2 => 'location',
        3 => 'offering',
        4 => 'person',
      ),
      'taxonomy' =>
      array (
        0 => 'all',
      ),
      'filters' =>
      array (
        0 => 'search',
      ),
      'result_elements' =>
      array (
        0 => 'featured_image',
        1 => 'post_type',
        2 => 'post_title',
      ),
      'max' => '',
      'field_group' => 230,
    ),
  ),
  'location' =>
  array (
    0 =>
    array (
      0 =>
      array (
        'param' => 'page_template',
        'operator' => '==',
        'value' => 'templates/pricing.php',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' =>
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' =>
    array (
    ),
  ),
  'menu_order' => 0,
);
