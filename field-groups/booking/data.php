<?php
$group = array(
    'id' => '537e662b73dab',
    'title' => 'Booking',
    'fields' => array(
        1 => array(
            'key' => 'field_537e5ac003ca3',
            'label' => 'Steps',
            'name' => 'steps',
            '_name' => 'steps',
            'type' => 'repeater',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-steps',
            'class' => 'repeater',
            'conditional_logic' => array(
                'status' => 0,
                'rules' => array(
                    0 => array(
                        'field' => 'null',
                        'operator' => '==',
                        'value' => ''
                    )
                ),
                'allorany' => 'all'
            ),
            'sub_fields' => array(
                array(
                    'key' => 'field_537e5ac903ca4',
                    'label' => 'Concierge Info',
                    'name' => 'concierge_info',
                    '_name' => 'concierge_info',
                    'type' => 'textarea',
                    'order_no' => 0,
                    'instructions' => 'Appears next to Concierge Image',
                    'required' => 0,
                    'id' => 'acf-field-concierge_info',
                    'class' => 'textarea',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'formatting' => 'br'
                ),
                array(
                    'key' => 'field_537e5aaheading',
                    'label' => 'Heading',
                    'name' => 'heading',
                    '_name' => 'heading',
                    'type' => 'text',
                    'order_no' => 1,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-heading',
                    'class' => 'text',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'formatting' => 'br'
                ),
                array(
                    'key' => 'field_537e5ad503ca5',
                    'label' => 'Info',
                    'name' => 'info',
                    '_name' => 'info',
                    'type' => 'textarea',
                    'order_no' => 2,
                    'instructions' => 'Appears below heading',
                    'required' => 0,
                    'id' => 'acf-field-info',
                    'class' => 'textarea',
                    'conditional_logic' => array(
                        'status' => 0,
                        'rules' => array(
                            0 => array(
                                'field' => 'null',
                                'operator' => '==',
                                'value' => ''
                            )
                        ),
                        'allorany' => 'all'
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'formatting' => 'br'
                )
            ),
            'row_min' => 3,
            'row_limit' => 3,
            'layout' => 'row',
            'button_label' => '',
            'field_group' => 351
        )
    ),
    'location' => array(
        0 => array(
            0 => array (
                'param' => 'page_template',
                'operator' => '==',
                'value' => 'templates/book.php',
                'order_no' => 0,
                'group_no' => 0,
            ),
        )
    ),
    'options' => array(
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array (
          0 => 'the_content',
          1 => 'excerpt',
          2 => 'custom_fields',
          3 => 'discussion',
          4 => 'comments',
          5 => 'author',
        ),
    ),
    'menu_order' => 0
);
