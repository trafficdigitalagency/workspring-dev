<?php
$group = array (
    'id' => '537e662b3c8fd',
    'title' => 'Location General',
    'fields' =>
    array (
        array (
              'key' => 'field_538e4a87b0f56',
              'label' => 'Affiliate Location',
              'name' => 'affiliate',
              '_name' => 'affiliate',
              'type' => 'true_false',
              'order_no' => 1,
              'instructions' => '',
              'required' => 0,
              'id' => 'acf-field-affiliate',
              'class' => 'true_false',
              'conditional_logic' =>
              array (
                'status' => 0,
                'rules' =>
                array (
                  0 =>
                  array (
                    'field' => 'null',
                    'operator' => '==',
                  ),
                ),
                'allorany' => 'all',
              ),
              'message' => '',
              'default_value' => 0,
              'field_group' => 1760,
        ),
        array (
            'key' => 'field_537e0af599699',
            'label' => 'Address',
            'name' => 'address',
            '_name' => 'address',
            'type' => 'google_map',
            'order_no' => 0,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-address',
            'class' => 'google_map',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'center_lat' => '',
            'center_lng' => '',
            'zoom' => '',
            'height' => '',
            'field_group' => 232,
        ),
        array (
            'key' => 'field_537e7e377237d',
            'label' => 'Location Information Line 1',
            'name' => 'location_line_1',
            '_name' => 'location_line_1',
            'type' => 'text',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-location_line_1',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e7e3772dawef',
            'label' => 'Location Information Line 2',
            'name' => 'location_line_2',
            '_name' => 'location_line_2',
            'type' => 'text',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-location_line_2',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e7e377237qwer',
            'label' => 'Location Information Line 3',
            'name' => 'location_line_3',
            '_name' => 'location_line_3',
            'type' => 'text',
            'order_no' => 1,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-location_line_3',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e4c39e423f',
            'label' => 'Quote',
            'name' => 'quote',
            '_name' => 'quote',
            'type' => 'textarea',
            'order_no' => 3,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-quote',
            'class' => 'textarea',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'formatting' => 'br',
            'field_group' => 232,
        ),
        array (
            'key' => 'field_537e4c4daed1c',
            'label' => 'Links',
            'name' => 'links',
            '_name' => 'links',
            'type' => 'repeater',
            'order_no' => 4,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-links',
            'class' => 'repeater',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'sub_fields' =>
            array (

                array (
                    'key' => 'field_537e84bbb1bdf',
                    'label' => 'URL',
                    'name' => 'url',
                    '_name' => 'url',
                    'type' => 'text',
                    'order_no' => 0,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-url',
                    'class' => 'text',
                    'conditional_logic' =>
                    array (
                        'status' => 0,
                        'rules' =>
                        array (

                            array (
                                'field' => 'null',
                                'operator' => '==',
                                'value' => '',
                            ),
                        ),
                        'allorany' => 'all',
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'none',
                    'maxlength' => '',
                ),

                array (
                    'key' => 'field_537e85338bf92',
                    'label' => 'File',
                    'name' => 'file',
                    '_name' => 'file',
                    'type' => 'file',
                    'order_no' => 1,
                    'instructions' => 'Takes precedence over URL',
                    'required' => 0,
                    'id' => 'acf-field-file',
                    'class' => 'file',
                    'conditional_logic' =>
                    array (
                        'status' => 0,
                        'rules' =>
                        array (

                            array (
                                'field' => 'field_537e84dd3c072',
                                'operator' => '==',
                                'value' => '_self',
                            ),
                        ),
                        'allorany' => 'all',
                    ),
                    'column_width' => '',
                    'save_format' => 'url',
                    'library' => 'all',
                ),

                array (
                    'key' => 'field_537e84c3b1be0',
                    'label' => 'Label',
                    'name' => 'label',
                    '_name' => 'label',
                    'type' => 'text',
                    'order_no' => 2,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-label',
                    'class' => 'text',
                    'conditional_logic' =>
                    array (
                        'status' => 0,
                        'rules' =>
                        array (

                            array (
                                'field' => 'null',
                                'operator' => '==',
                                'value' => '',
                            ),
                        ),
                        'allorany' => 'all',
                    ),
                    'column_width' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => '',
                ),

                array (
                    'key' => 'field_537e84dd3c072',
                    'label' => 'Target',
                    'name' => 'target',
                    '_name' => 'target',
                    'type' => 'select',
                    'order_no' => 3,
                    'instructions' => '',
                    'required' => 0,
                    'id' => 'acf-field-target',
                    'class' => 'select',
                    'conditional_logic' =>
                    array (
                        'status' => 0,
                        'rules' =>
                        array (

                            array (
                                'field' => 'null',
                                'operator' => '==',
                                'value' => '',
                            ),
                        ),
                        'allorany' => 'all',
                    ),
                    'column_width' => '',
                    'choices' =>
                    array (
                        '_self' => 'Same Window',
                        '_blank' => 'New Window',
                    ),
                    'default_value' => '',
                    'allow_null' => 0,
                    'multiple' => 0,
                ),
            ),
            'row_min' => '',
            'row_limit' => '',
            'layout' => 'row',
            'button_label' => 'Add Link',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e4c68cb7ab',
            'label' => 'Hours',
            'name' => 'hours',
            '_name' => 'hours',
            'type' => 'wysiwyg',
            'order_no' => 6,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-hours',
            'class' => 'wysiwyg',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'toolbar' => 'full',
            'media_upload' => 'yes',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e4c7dcb7ac',
            'label' => 'Contact',
            'name' => 'contact',
            '_name' => 'contact',
            'type' => 'wysiwyg',
            'order_no' => 7,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-contact',
            'class' => 'wysiwyg',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'toolbar' => 'full',
            'media_upload' => 'yes',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e4c86cb7ad',
            'label' => 'Phone Number',
            'name' => 'phone_number',
            '_name' => 'phone_number',
            'type' => 'text',
            'order_no' => 8,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-phone_number',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'none',
            'maxlength' => '',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e4c97cb7ae',
            'label' => 'Email',
            'name' => 'email',
            '_name' => 'email',
            'type' => 'text',
            'order_no' => 9,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-email',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'none',
            'maxlength' => '',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e4ca64af35',
            'label' => 'Tour URL',
            'name' => 'tour_url',
            '_name' => 'tour_url',
            'type' => 'text',
            'order_no' => 10,
            'instructions' => 'Formatted like "https://maps.google.com/maps?hl=en-US..."',
            'required' => 0,
            'id' => 'acf-field-tour_url',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'none',
            'maxlength' => '',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e8e8c6755f',
            'label' => 'Tour Image',
            'name' => 'tour_image',
            '_name' => 'tour_image',
            'type' => 'image',
            'order_no' => 11,
            'instructions' => 'Displayed on mobile in place of tour',
            'required' => 0,
            'id' => 'acf-field-tour_image',
            'class' => 'image',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                    ),
                ),
                'allorany' => 'all',
            ),
            'save_format' => 'id',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e4d3035455',
            'label' => 'Video Title',
            'name' => 'video_title',
            '_name' => 'video_title',
            'type' => 'text',
            'order_no' => 12,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-video_title',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e8c6735313',
            'label' => 'Video Image',
            'name' => 'video_image',
            '_name' => 'video_image',
            'type' => 'image',
            'order_no' => 13,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-video_image',
            'class' => 'image',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'save_format' => 'id',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'field_group' => 232,
        ),

        array (
            'key' => 'field_537e4d2335454',
            'label' => 'Video URL',
            'name' => 'video_url',
            '_name' => 'video_url',
            'type' => 'text',
            'order_no' => 14,
            'instructions' => '',
            'required' => 0,
            'id' => 'acf-field-video_url',
            'class' => 'text',
            'conditional_logic' =>
            array (
                'status' => 0,
                'rules' =>
                array (

                    array (
                        'field' => 'null',
                        'operator' => '==',
                        'value' => '',
                    ),
                ),
                'allorany' => 'all',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'none',
            'maxlength' => '',
            'field_group' => 232,
        ),
    ),
    'location' =>
    array (

        array (

            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'location',
                'order_no' => 0,
                'group_no' => 0,
            ),
        ),
    ),
    'options' =>
    array (
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' =>
        array (
             'the_content',
        ),
    ),
    'menu_order' => 0,
);
