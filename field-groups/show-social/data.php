<?php
if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_about-page',
    'title' => 'Show Share',
    'fields' => array (
      array (
        'key' => 'field_53baf7c0f6217',
        'label' => 'Show Sharing',
        'name' => 'show_sharing',
        'type' => 'true_false',
        'message' => '',
        'default_value' => 0,
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'page',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array (
      ),
    ),
    'menu_order' => 0,
  ));
}
