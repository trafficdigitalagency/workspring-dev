<?php
function SOP_addOptimization($rules)
{
    $optimizationParams = array(
        "\n# BEGIN SOPDeflate",
        "\nAddOutputFilterByType DEFLATE text/plain",
        "AddOutputFilterByType DEFLATE text/html",
        "AddOutputFilterByType DEFLATE text/xml",
        "AddOutputFilterByType DEFLATE text/css",
        "AddOutputFilterByType DEFLATE application/xml",
        "AddOutputFilterByType DEFLATE application/xhtml+xml",
        "AddOutputFilterByType DEFLATE application/rss+xml",
        "AddOutputFilterByType DEFLATE application/javascript",
        "AddOutputFilterByType DEFLATE application/x-javascript",
        "\n# END SOPDeflate",
        "\n# BEGIN SOPExpires",
        "\nExpiresActive On",
        "ExpiresByType image/jpg \"access plus 1 year\"",
        "ExpiresByType image/jpeg \"access plus 1 year\"",
        "ExpiresByType image/gif \"access plus 1 year\"",
        "ExpiresByType image/png \"access plus 1 year\"",
        "ExpiresByType text/css \"access plus 1 year\"",
        "ExpiresByType application/pdf \"access plus 1 year\"",
        "ExpiresByType text/x-javascript \"access plus 1 year\"",
        "ExpiresByType application/javascript \"access plus 1 year\"",
        "ExpiresByType application/x-shockwave-flash \"access plus 1 year\"",
        "ExpiresByType image/svg+xml \"access plus 1 year\"",
        "ExpiresByType application/x-font-ttf \"access plus 1 year\"",
        "ExpiresByType application/x-font-truetype \"access plus 1 year\"",
        "ExpiresByType application/x-font-opentype \"access plus 1 year\"",
        "ExpiresByType application/font-woff \"access plus 1 year\"",
        "ExpiresByType font/opentype \"access plus 1 year\"",
        "ExpiresByType image/x-icon \"access plus 1 year\"",
        "ExpiresDefault \"access plus 2 days\"",
        "\n# END SOPExpires",
    );

    $search = "<IfModule mod_rewrite.c>";

    $rules = str_replace($search, implode("\n", $optimizationParams) . "\n\n" . $search, $rules);

    return $rules;
}
