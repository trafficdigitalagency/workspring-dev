<?php

namespace someoddpilot\Template;

class LoopTemplate
{
    private $priority;

    private $loopIndex = 0;

    public function __construct($priority = 10)
    {
        $this->priority = $priority;

        add_action("someoddpilot\LoopTemplate\loop", array($this, "loop"), $this->priority, 3);

        add_filter("someoddpilot\LoopTemplate\loopIndex", array($this, "getLoopIndex"), $this->priority);
    }

    public function loop($template, $conditionCallback, $conditionArgs = array())
    {
        $this->loopIndex = -1;

        while (call_user_func_array($conditionCallback, $conditionArgs)) {
            $this->loopIndex++;
            get_template_part($template);
        }

        $this->loopIndex = false;
    }

    public function getLoopIndex()
    {
        return $this->loopIndex;
    }
}
