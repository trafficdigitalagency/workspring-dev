<?php

function SOP_facebookShareUrl($url)
{
    return "http://www.facebook.com/sharer/sharer.php?u=" . urlencode($url);
}

function SOP_twitterShareUrl($url, $title = "")
{
    $string = (!empty($title)) ? $title . " - " . $url : $url;

    return "https://twitter.com/home?status="
        . urlencode($string);
}

function SOP_linkedInShareUrl($url, $title = "", $excerpt = "")
{
    return "https://www.linkedin.com/shareArticle?mini=true" . http_build_query(
        array(
            "source" => 'Workspring',
            "title" => $title,
            "url" => $url,
            "summary" => substr($excerpt, 0, 200)
        )
    );
}

function SOP_tumblrShareUrl($url, $title = "")
{
    return "http://tumblr.com/share?" . http_build_query(
        array(
            "s" => "",
            "v" => 3,
            "t" => $title,
            "u" => $url,
        )
    );
}
