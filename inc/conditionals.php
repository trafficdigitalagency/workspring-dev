<?php

/**
 * Tests if page is in iframe
 */
function SOP_isiFrame()
{
    return (!empty($_GET['iframe']));
}
