<?php
/**
 * About Nav Menu Walker
 */

namespace someoddpilot\Walker;

/**
 * Removes crud from menu output
 */
class AboutWalker extends \Walker_Nav_Menu
{
    public function end_el(&$output, $item, $depth = 0, $args = array())
    {
        $output .= "</li>";
    }
}
