<?php

function SOP_postSortingLink($values)
{
    if (is_post_type_archive("person") || is_tax('role')) {
        $params = array(
            "location-id",
            "role-id"
        );

        $baseUrl = get_post_type_archive_link("person");
    } else {
        $params = array(
            "location-id",
            "category-id"
        );

        $baseUrl = get_permalink(get_option("page_for_posts"));
    }

    $currentValues = array_combine(
        $params,
        array_map(
            function ($paramName) {
                return $_GET[$paramName];
            },
            $params
        )
    );

    $itemParams = wp_parse_args(
        $values,
        $currentValues
    );

    return $baseUrl . "?" . http_build_query($itemParams);
}

function SOP_termSortingLabel($param, $taxonomy, $default)
{
    if (empty($_GET[$param])) {
        return $default;
    }

    $term = get_term($_GET[$param], $taxonomy);

    if (empty($term)) {
        return $default;
    }

    return $term->name;
}

function SOP_postSortingLabel($param, $default)
{
    if (empty($_GET[$param])) {
        return $default;
    }

    $post = get_post($_GET[$param]);

    if (empty($post)) {
        return $default;
    }

    return $post->post_title;
}

function SOP_filterer($label, $allUrl, $options)
{
    ?>
    <div class="col-md-4 post-filterer">
        <div class="dropdown">
            <button class="btn btn-secondary" data-toggle="dropdown">
                <?php echo $label; ?>
            </button>
            <ul class="dropdown-menu">
                <li class="dropdown-item">
                    <a href="<?php echo $allUrl; ?>">
                        All
                    </a>
                </li>
                <?php
                foreach ($options as $option) {
                    ?>
                    <li class="dropdown-item">
                        <a href="<?php echo $option["url"]; ?>">
                            <?php echo $option["label"]; ?>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <?php
}

function SOP_taxomonyFilter($taxonomy, $default)
{
    $param = $taxonomy . "-id";

    $label = SOP_termSortingLabel($param, $taxonomy, $default);

    $allUrl = SOP_postSortingLink(array($param => null));

    $options = array();

    foreach (get_terms($taxonomy) as $term) {
        $options[] = array(
            "url"   => SOP_postSortingLink(array($param => $term->term_id)),
            "label" => $term->name,
        );
    }

    SOP_filterer($label, $allUrl, $options);
}

function SOP_locationFilter()
{
    $label = SOP_postSortingLabel("location-id", "Filter by Location");

    $allUrl = SOP_postSortingLink(array("location-id" => null));

    $options = array_map(
        function ($post) {
            return array(
                "label" => $post->post_title,
                "url"   => SOP_postSortingLink(array("location-id" => $post->ID))
            );
        },
        get_posts(
            array(
                'post_type'      => 'location',
                'posts_per_page' => -1,
                "orderby"        => "title",
                "order"          => "ASC",
            )
        )
    );

    SOP_filterer($label, $allUrl, $options);
}
