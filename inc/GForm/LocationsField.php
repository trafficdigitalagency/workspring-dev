<?php

namespace someoddpilot\GForm;

class LocationsField extends PopulateField
{
    public $options = array();

    public $selectText = "Select a location";

    public $fieldType = "populate-locations";

    public function getOptions()
    {
        return apply_filters('someoddpilot\Location\getLocations', array());
    }
}
