<?php

namespace someoddpilot\GForm;

abstract class PopulateField
{
    public $options = array();

    public $selectText = "Select an item";

    public $fieldType = "populate-items";

    public function __construct()
    {
        add_filter('gform_pre_render', array($this, 'populateChoices'));

        $this->options = $this->getOptions();
    }

    abstract protected function getOptions();

    public function setOptions($options = array())
    {
        $this->options = $options;
    }

    public function populateChoices($form = array())
    {
        if (empty($form) || empty($form['fields'])) {
            return $form;
        }

        $form['fields'] = array_map(array($this, "populateFieldValues"), $form['fields']);

        return $form;
    }

    public function populateFieldValues($field)
    {
        if (!$this->isPopulationField($field)) {
            return $field;
        }

        $field['choices'] = array_map(
            array($this, "mapNamesToChoices"),
            $this->options
        );

        if (!empty($this->selectText)) {
            array_unshift(
                $field['choices'],
                array(
                    'text'  => $this->selectText,
                    'value' => '',
                )
            );
        }

        return $field;
    }

    private function isPopulationField($field)
    {
        if (empty($field['cssClass']) || $field['type'] !== 'select') {
            return false;
        }
        return (strpos($field['cssClass'], $this->fieldType) !== false );
    }

    public function mapNamesToChoices($choice)
    {
        return array(
            'text' => $choice['name'],
            'value' => $choice['id'],
        );
    }
}
