<?php

namespace someoddpilot\GForm;

class SkipPage
{
    public function __construct()
    {
        add_filter('gform_pre_render', array($this, 'skipGFormPage'));
    }

    /**
    * Skip Pages on Multi-Page Form
    * http://gravitywiz.com/2012/05/04/pro-tip-skip-pages-on-multi-page-forms/
    *
    * @param array $form form
    *
    * @return array form
    */
    public function skipGFormPage($form = array())
    {
        if ($this->isValidToSkip($form)) {
            \GFFormDisplay::$submission[$form['id']]["page_number"] = rgget('form-page');
        }

        return $form;
    }

    private function isValidToSkip($form)
    {
        $conditions = array(
            'empty title'    => empty($form['title']),
            'empty id'       => empty($form['id']),
            'empty page'     => !rgget('form-page'),
        );

        if (in_array(true, $conditions)) {
            return false;
        }

        return (!rgpost("is_submit_" . $form['id']));
    }
}
