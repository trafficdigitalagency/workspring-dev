<?php

namespace someoddpilot\GForm;

class GuestsField extends PopulateField
{
    public $options = array();

    public $selectText = false;

    public $fieldType = "populate-guests";

    public function getOptions()
    {
        return array_map(array($this, "createOption"), range(0, 49));
    }

    public function createOption($option)
    {
        return array(
            'name'  => $option,
            'id' => $option,
        );
    }
}
