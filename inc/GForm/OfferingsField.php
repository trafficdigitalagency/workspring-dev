<?php

namespace someoddpilot\GForm;

class OfferingsField extends PopulateField
{
    public $selectText = "Select a service";

    public $fieldType = "populate-offerings";

    public function getOptions()
    {
        return apply_filters('someoddpilot\Offering\getOfferings', array());
    }
}
