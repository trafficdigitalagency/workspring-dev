<?php

namespace someoddpilot\Calculator;

class Variable
{
    public $type = "";

    public $slug = "";

    public $options = array();

    public function __construct($values)
    {
        $this->type = $values["type"];

        $this->slug = sanitize_title($this->type);

        $options = array_map(
            function ($option) {
                return new VariableOption($option);
            },
            $values["options"]
        );

        $options = array_combine(
            array_map(
                function ($options) {
                    return $options->slug;
                },
                $options
            ),
            $options
        );

        $this->options = $options;
    }
}
