<?php

namespace someoddpilot\Calculator;

class VariableOption
{
    public $label = "";

    public $slug = "";

    public $multiplier = 0;

    public $unit = "";

    public function __construct($values)
    {
        $this->label = $values["label"];

        $this->multiplier = $values["multiplier"];

        $this->unit = $values["unit"];

        $this->slug = sanitize_title($this->label);
    }
}
