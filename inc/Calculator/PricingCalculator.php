<?php

namespace someoddpilot\Calculator;

class PricingCalculator
{
    private $variables = array();

    private $values = array();

    public function __construct($variables = array())
    {
        $variables = array_map(function ($variable) {
            return new Variable($variable);
        }, $variables);

        $variables = array_combine(
            array_map(
                function ($variable) {
                    return $variable->slug;
                },
                $variables
            ),
            $variables
        );

        $this->variables = $variables;

        $this->values = array_map(
            function () {
                return null;
            },
            $this->variables
        );
    }

    public function setValue($type, $value)
    {
        if (empty($this->variables[$type]) || empty($this->variables[$type]->options[$value])) {
            return;
        }

        $this->values[$type] = $this->variables[$type]->options[$value];
    }

    private function getPrice()
    {
        $price = 1;

        foreach ($this->values as $value) {
            $price = $price * $value->multiplier;
        }

        return $price;
    }

    private function getUnit()
    {
        $units = array_map(function ($value) {
            return $value->unit;
        }, $this->values);

        return trim(implode(" ", $units));
    }

    public function getCalculatedPrice()
    {
        $price = $this->getPrice();

        $unit = $this->getUnit();

        if (empty($price)) {
            return "We’ll contact you to discuss options tailored to your team’s unique needs.";
        }

        if (!empty($unit)) {
            $unit = " " . $unit;
        }

        return $price . $unit;
    }
}
