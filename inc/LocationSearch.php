<?php
/**
 * Location Search Class
 */

namespace someoddpilot;

class LocationSearch
{
    private $geoCode;

    private $locations = array();

    /**
     * get closest locations
     *
     * @param string $address address
     *
     * @return array          locations
     */
    public function getClosestLocations($address = "")
    {
        if (empty($this->locations)) {
            return $this->locations;
        }

        $geoCode = $this->getGeoCode($address);

        if (empty($geoCode)) {
            return $this->locations;
        }

        $this->geoCode = $geoCode;

        $this->locations = array_map(array($this, "mapDistance"), $this->locations);

        usort($this->locations, array($this, 'compare'));

        return $this->locations;
    }

    public function mapDistance($location)
    {
        $location['distance'] = $this->distanceBetweenPoints(
            floatval($this->geoCode[0]->geometry->location->lat),
            floatval($this->geoCode[0]->geometry->location->lng),
            $location['lat'],
            $location['lng']
        );

        return $location;
    }

    /**
     * set locations
     *
     * @param array $locations array of locations
     *
     * @return void
     */
    public function setLocations($locations)
    {
        $this->locations = $locations;
    }

    /**
     * compare location distances
     * return 0  if equal
     *        -1 if location A is closer
     *        1  if location B is closer
     *
     * @param Array $locationA location A
     * @param Array $locationB location B
     *
     * @return Integer    comparison
     */
    public function compare($locationA, $locationB)
    {
        if ($locationA['distance'] == $locationB['distance']) {
            return 0;
        }
        return ($locationA['distance'] < $locationB['distance']) ? -1 : 1;
    }

    /**
     * get Google geocode data
     *
     * @param string $address address
     *
     * @return Object google geocode response
     */
    private function getGeoCode($address)
    {
        $query = http_build_query(
            array(
                'sensor' => 'false',
                'address' => $address,
            )
        );

        $response = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?" . $query);

        $response = json_decode($response);

        if ('OK' === $response->status) {
            return $response->results;
        }

        return array();
    }

    /**
     * compute distance between two lat/long points
     *
     * uses the ‘haversine’ formula
     *
     * http://www.movable-type.co.uk/scripts/latlong.html
     *
     * @param integer $latA latitude coordinate
     * @param integer $lonA latitude coordinate
     * @param integer $latB latitude coordinate
     * @param integer $lonB latitude coordinate
     *
     * @return double
     */
    private function distanceBetweenPoints($latA, $lonA, $latB, $lonB)
    {
        $earthsRadiusInMiles = 6371;
        $dLat                = deg2rad($latB - $latA);
        $dLon                = deg2rad($lonB - $lonA);
        $lat1                = deg2rad($latA);
        $lat2                = deg2rad($latB);

        $arc         = sin($dLat/2) * sin($dLat/2) + sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2);
        $partialCirc = 2 * atan2(sqrt($arc), sqrt(1 - $arc));

        return $earthsRadiusInMiles * $partialCirc;
    }
}
