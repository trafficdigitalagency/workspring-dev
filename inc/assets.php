<?php
/**
 * Image Functions
 */

function SOP_imageUri($string)
{
    return get_template_directory_uri() . '/img/' . $string;
}

function SOP_addImageSizes()
{
    $sizes = array(
        array(
            'name'   => 'choose-location',
            'width'  => 1600,
            'height' => 410,
        ),
        array(
            'name'   => 'concierge',
            'width'  => 72,
            'height' => 72,
        ),
        array(
            'name'   => 'front-page-news-slider',
            'width'  => 450,
            'height' => 490,
        ),
        array(
            'name'   => 'front-page-slider',
            'width'  => 1600,
            'height' => 465,
        ),
        array(
            'name'   => 'large-news-image',
            'width'  => 633,
            'height' => 468,
        ),
        array(
            'name'   => 'location-video',
            'width'  => 980,
            'height' => 475,
        ),
        array(
            'name'   => 'mobile-header',
            'width'  => 320,
            'height' => 212,
        ),
        array(
            'name'   => 'mobile-front-page-slider',
            'width'  => 320,
            'height' => 402,
        ),
        array(
            'name'   => 'mobile-front-page-news-slider',
            'width'  => 320,
            'height' => 350,
        ),
        array(
            'name'   => 'mobile-location-image',
            'width'  => 264,
            'height' => 191,
        ),
        array(
            'name'   => 'mobile-large-news-image',
            'width'  => 264,
            'height' => 350,
        ),
        array(
            'name'   => 'news-image',
            'width'  => 284,
            'height' => 210,
        ),
        array(
            'name'   => 'offering-image',
            'width'  => 234,
            'height' => 234,
        ),
        array(
            'name'   => 'page-header',
            'width'  => 1600,
            'height' => 250,
        ),
        array(
            'name'   => 'person',
            'width'  => 293,
            'height' => 442,
        ),
        array(
            'name'   => 'post-detail',
            'width'  => 634,
            'height' => 634,
            'crop'   => false
        ),
        array(
            'name'   => 'post-slider',
            'width'  => 980,
            'height' => 9999,
        ),
    );

    foreach ($sizes as $imageSize) {
        $imageSize['crop'] = true;
        call_user_func_array("add_image_size", $imageSize);
    }
}

function SOP_fileSuffix()
{
    if (!SOP_isDev()) {
        return ".min";
    }

    return "";
}

/**
 * Enqueue scripts
 *
 * @return void
 */
function WSP_enqueue()
{
    if (!SOP_isDev()) {
        wp_deregister_script('jquery');
        wp_register_script(
            'jquery',
            '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
            array(),
            "1.11.1"
        );
    }

    wp_enqueue_script("jquery");

    wp_deregister_script('underscore');
    wp_deregister_script('backbone');
    wp_deregister_script('_gf_placeholders');

    wp_enqueue_style(
        'workspring-style',
        get_stylesheet_directory_uri() . '/css/style' . SOP_fileSuffix() . '.css',
        array(),
        '2.1.0'
    );

}

function SOP_isDev()
{
    return $_SERVER['HTTP_HOST'] === '10.1.10.181';
}

function SOP_mainUri()
{
    $suffix = (!SOP_isDev()) ? "-built" : "";

    return site_url() . "/wp-content/themes/workspring/js/main" . $suffix;
}

function SOP_localizeScript()
{
    return array_merge(
        SOP_bootstrapOfferings(),
        array(
            'mainL10n' => array(
                'baseUrl' => site_url(),
                'apiUrl'  => site_url('/api'),
                'book'    => SOP_bookL10n(),
            ),
            'waterfall' => array(
                'apiUrl' => admin_url('admin-ajax.php')
            )
        )
    );
}

function SOP_bookL10n()
{
    return array(
        'location' => ($_POST['input_1']) ? $_POST['input_1'] : $_GET['book-location'],
        'offering' => ($_POST['input_3']) ? $_POST['input_3'] : $_GET['book-offering'],
    );
}
