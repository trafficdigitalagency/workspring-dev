<?php

function SOP_getLinkURL()
{
    return (get_sub_field("file")) ? get_sub_field("file") : get_sub_field("url");
}

function SOP_getAddressPart($part, $postID = 0)
{
    if (empty($postID)) {
        $postID = get_the_id();
    }

    $address_components = get_post_meta($postID, "address_components", true);

    $value = (! empty($address_components->$part)) ? $address_components->$part : "";

    if (get_post_meta($postID, "override_" . $part, true)) {
        $value = get_post_meta($postID, "override_" . $part, true);
    }

    return $value;
}

function SOP_getTourUrl($url)
{
    if (!preg_match("/source=embed/", $url)) {
        $url .= "&amp;source=embed";
    }

    if (!preg_match("/output=svembed/", $url)) {
        $url .= "&amp;output=svembed";
    }

    return $url;
}

function SOP_mapsEmebed($address)
{
    return "https://www.google.com/maps/embed/v1/place?" . http_build_query(
        array(
            "q"   => $address['address'],
            "key" => "AIzaSyD09pBNe9Ic8kMI3TfXygPnsTzFxNPJCe0",
        )
    );
}

function SOP_directionsUrl($address)
{
    return "https://www.google.com/maps/place/" . urlencode($address['address']);
}

function SOP_staticMapUri($address, $width = 600, $height = 300)
{
    return "http://maps.googleapis.com/maps/api/staticmap?" . http_build_query(
        array(
            "center"  => $address['address'],
            "zoom"    => 14,
            "size"    => $width . "x" . $height,
            "maptype" => "roadmap",
            "markers" => "color:orange|" . $address,
            "sensor"  => false,
            "key"     => "AIzaSyD09pBNe9Ic8kMI3TfXygPnsTzFxNPJCe0",
            "scale"   => 2,
        )
    );
}

function SOP_registerFieldGroup($name)
{
    $group = array();

    require_once __DIR__ . '/../field-groups/' . $name . '/data.php';

    register_field_group($group);
}

function SOP_cleanPhone($number)
{
    return preg_replace('/\D/', '', $number);
}

function SOP_getThumbnailSrc($size)
{
    list($src) = wp_get_attachment_image_src(get_post_thumbnail_id(), $size);
    return $src;
}

function SOP_videoClass()
{
    if (get_sub_field("video")) {
        return "video";
    }
}

function SOP_slideModalAttr()
{
    $attributes = "";

    if (get_sub_field("video") || get_sub_field("modal")) {
        $attributes .= "modal";

        if (get_sub_field("video")) {
            $attributes .= " modal-video";
        }
    }

    return $attributes;
}

function SOP_getGformPage()
{
    $page_number = \GFFormDisplay::$submission[get_field('form')->id]["page_number"];

    if (empty($page_number)) {
        if (!empty(\GFFormDisplay::$submission)) {
            $page_number = 3;
        } else {
            $page_number = 1;
        }
    }

    return $page_number - 1;
}

function SOP_enqueueForm()
{
    if (get_field('form')) {
        gravity_form(
            get_field('form')->id,
            false,
            false,
            true
        );
    } // endif form
}

function SOP_getOfferingPricing($offerings)
{
    return array_map(function ($offering) {
        $pricingData = array(
            'id'      => $offering->ID,
            'info'    => get_field('additional_info', $offering),
            'slug'    => $offering->post_name,
            'ctas'    => get_field("ctas", $offering),
            'pricing' => array_map(
                function ($pricing) {
                    $pricing['variables'] = array_map(
                        function ($variable) {
                            $variable['slug'] = sanitize_title($variable['type']);
                            $variable['options'] = array_map(
                                function ($option) {
                                    $option['slug'] = sanitize_title($option['label']);
                                    return $option;
                                },
                                $variable['options']
                            );
                            return $variable;
                        },
                        $pricing['variables']
                    );
                    return $pricing;
                },
                get_field("location_pricing", $offering->ID)
            ),
            'estimateText' => get_field("email_estimate", $offering->ID),
        );

        return $pricingData;
    }, $offerings);
}

function SOP_getBookHeading()
{
    return SOP_getBookText("heading", "header");
}

function SOP_getBookText($identifier, $field)
{
    $steps = get_field("steps");

    $stepNumber = SOP_getGformPage();

    $info = $steps[$stepNumber][$identifier];

    if ($stepNumber == 1) {
        switch ($_POST['input_3']) {
            case "meeting-or-event":
                $info = get_field($field, 755);
                break;
            case "coworking-or-team-space":
                $info = get_field($field, 236);
                break;
            default:
                break;
        }
    }

    return $info;
}

function SOP_getBookInfo()
{
    return SOP_getBookText("info", "subcopy");
}

function SOP_getPostCategories()
{
    return wp_get_post_categories(
        get_the_ID(),
        array(
            "fields" => "all",
        )
    );
}

function SOP_bookCTAUrl()
{
    $url = get_field("booking_page", "options") . "?";

    $params = get_sub_field("parameters");

    $paramString = http_build_query(
        array_combine(
            array_map(
                function ($value) {
                    return $value["key"];
                },
                $params
            ),
            array_map(
                function ($value) {
                    return $value["value"];
                },
                $params
            )
        )
    );

    return $url . $paramString;
}

function SOP_featuredImageLinkAttr()
{
    $url = get_permalink();
    $addAttr = "";

    if (get_field("featured_video")) {
        $url = get_field("featured_video");
        $addAttr = " modal modal-video class='video-link'";
    }

    return 'href="' . $url . '"' . $addAttr;
}

function SOP_slideBackground($size = "front-page-slider")
{
    list($src) = wp_get_attachment_image_src(
        get_sub_field("image"),
        $size
    );

    return $src;
}

function SOP_backgroundImage($imageId, $size)
{
    list($src) = wp_get_attachment_image_src(
        $imageId,
        $size
    );
    return 'style="background-image: url(' . $src . ');"';
}

function SOP_backUrl()
{
    if (is_singular("post")) {
        return get_permalink(get_option("page_for_posts"));
    }
    $ancestors = get_post_ancestors(get_the_id());
    if (!empty($ancestors)) {
        return get_permalink(array_shift($ancestors));
    }
    return site_url();
}
