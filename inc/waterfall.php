<?php

function WSP_waterfall()
{
    $urlParts = parse_url($_GET['from']);

    $params = wp_parse_args($urlParts['query']);

    $locationId = ($params['location-id']) ? $params['location-id'] : null;
    $categoryId = ($params['category-id']) ? $params['category-id'] : null;

    $sticky = get_option('sticky_posts');

    $query_args = array(
        'post_type'           => 'post',
        'ignore_sticky_posts' => true,
        'post_status'         => 'publish',
        'offset'              => ($_GET['page'] * get_option('posts_per_page')),
    );

    $stickyPost = get_posts(
        array(
            'posts_per_page' => 1,
            'post__in' => $sticky
        )
    );

    if (!empty($stickyPost)) {
        $query_args['post__not_in'] = array($stickyPost[0]->ID);
        $query_args['offset'] = $query_args['offset'] + 1;
    }

    if (!empty($locationId)) {
        $query_args["connected_type"]  = "posts_to_locations";
        $query_args["connected_items"] = intval($locationId);
    }

    if (!empty($categoryId)) {
        $query_args['tax_query'] = array(
            array(
                "taxonomy" => "category",
                "field"    => "term_id",
                "terms"    => $categoryId
            )
        );
    }

    $waterfallPosts = new WP_Query($query_args);

    if ($waterfallPosts->have_posts()) {
        while ($waterfallPosts->have_posts()) {
            $waterfallPosts->the_post();
            ?>
            <div class="col-md-4 listing-item <?php echo SOP_nthChildClass(SOP_getLoopIndex()); ?>">
                <?php get_template_part("partials/listing-item"); ?>
            </div>
            <?php
        }
    }
    exit;
}
