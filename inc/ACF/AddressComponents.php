<?php

namespace someoddpilot\ACF;

use someoddpilot\LocationAddress;

class AddressComponents
{
    public function __construct()
    {
        add_filter('acf/update_value/type=google_map', array($this, 'addMapAddressComponents'), 10, 2);

        add_filter('someoddpilot\getAddressPart', array($this, 'getAddressPart'), 10, 2);
    }

    public function addMapAddressComponents($value, $postId)
    {
        update_post_meta($postId, "address_components", new LocationAddress\LocationAddress($value['address']));

        return $value;
    }
}
