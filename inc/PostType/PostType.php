<?php
/**
 *
 */

namespace someoddpilot\PostType;

abstract class PostType
{
    public $name = "";

    public $args = array();

    abstract public function setUp();

    public function __construct()
    {
        add_action('init', array($this, 'registerPostType'));

        $this->setUp();
    }

    private function getFullArgs()
    {
        return array_replace_recursive(
            array(
                'hierarchical'      => false,
                'public'            => true,
                'show_in_nav_menus' => true,
                'show_ui'           => true,
                'supports'          => array(
                    'title',
                    'editor',
                    'thumbnail',
                    'excerpt',
                    'revisions',
                    'page-attributes'
                ),
                'has_archive'       => true,
                'rewrite'           => true,
            ),
            $this->args
        );
    }

    /**
    * Registers a offering post type
    *
    * @return void
    */
    public function registerPostType()
    {
        register_post_type(
            $this->name,
            $this->getFullArgs()
        );
    }
}
