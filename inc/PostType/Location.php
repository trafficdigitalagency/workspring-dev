<?php
/**
 * Location Post Type
 */

namespace someoddpilot\PostType;

class Location extends PostType
{
    public $name = "location";

    public $args = array(
        'query_var'         => 'location',
        'supports'          => array(
            'title',
            'thumbnail',
            'excerpt',
            'revisions',
            'page-attributes'
        ),
        'labels'            => array(
            'name'               => 'Locations',
            'singular_name'      => 'Location',
            'all_items'          => 'Locations',
            'new_item'           => 'New location',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New location',
            'edit_item'          => 'Edit location',
            'view_item'          => 'View location',
            'search_items'       => 'Search locations',
            'not_found'          => 'No locations found',
            'not_found_in_trash' => 'No locations found in trash',
            'parent_item_colon'  => 'Parent location',
            'menu_name'          => 'Locations',
            'name_admin_bar'     => 'Locations',
        ),
    );

    public function setUp()
    {
        add_filter('someoddpilot\Location\getLocations', array($this, "getLocations"));
    }

    public function getLocations()
    {
        $args = array(
            'post_type' => 'location',
            'posts_per_page' => -1,
        );

        $location_query = new \WP_Query($args);

        $locations = array();

        if (empty($location_query->posts)) {
            return $locations;
        }

        foreach ($location_query->posts as $location) {
            $location_data = \get_field('address', $location->ID);

            $locations[] = array(
                'name' => $location->post_title,
                'id' => $location->ID,
                'lat' => $location_data['lat'],
                'lng' => $location_data['lng'],
                'permalink' => get_permalink($location->ID),
            );
        }

        return $locations;
    }
}
