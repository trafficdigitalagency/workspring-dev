<?php
/**
 * Location Post Type
 */

namespace someoddpilot\PostType;

class Person extends PostType
{
    public $name = "person";

    public $args = array(
        'rewrite' => array(
            'slug' => 'people',
        ),
        'query_var'         => 'person',
        'supports'          => array(
            'title',
            'thumbnail',
            'editor',
            'excerpt',
            'revisions',
            'page-attributes'
        ),
        'labels'            => array(
            'name'               => 'People',
            'singular_name'      => 'Person',
            'all_items'          => 'People',
            'new_item'           => 'New Person',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Person',
            'edit_item'          => 'Edit Person',
            'view_item'          => 'View Person',
            'search_items'       => 'Search People',
            'not_found'          => 'No People found',
            'not_found_in_trash' => 'No People found in trash',
            'parent_item_colon'  => 'Parent Person',
            'menu_name'          => 'People',
            'name_admin_bar'     => 'People',
        ),
    );

    public function setUp()
    {
        add_filter("someoddpilot\Person\getPeople", array($this, "getPeople"));
    }

    public function getPeople()
    {
        return array();
    }
}
