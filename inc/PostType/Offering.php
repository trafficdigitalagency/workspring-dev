<?php
/**
 * Offering post type
 */

namespace someoddpilot\PostType;

class Offering extends PostType
{
    public $name = "offering";

    public $args = array(
        'public'            => false,
        'supports'          => array(
            'title',
            'thumbnail',
            'revisions',
            'page-attributes'
        ),
        'labels'            => array(
            'name'               => 'Offerings',
            'singular_name'      => 'Offering',
            'all_items'          => 'Offerings',
            'new_item'           => 'New Offering',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Offering',
            'edit_item'          => 'Edit Offering',
            'view_item'          => 'View Offering',
            'search_items'       => 'Search Offerings',
            'not_found'          => 'No Offerings found',
            'not_found_in_trash' => 'No Offerings found in trash',
            'parent_item_colon'  => 'Parent Offering',
            'menu_name'          => 'Offerings',
            'name_admin_bar'     => 'Offerings',
        ),
    );

    public function setUp()
    {
        add_filter('someoddpilot\Offering\getOfferings', array($this, "getOfferings"));
    }

    public function getOfferingLocations()
    {

    }

    public function getOfferings()
    {
        $offering_query = new \WP_Query(
            array(
                'posts_per_page' => -1,
                'post_type' => 'offering',
            )
        );

        $offerings = array();

        if (empty($offering_query->posts)) {
            return $offerings;
        }

        foreach ($offering_query->posts as $offering) {
            $offering_locations = get_field('locations', $offering->ID);

            $locations = array();

            if (! empty($offering_locations)) {
                $locations = array_map(array($this, "mapLocations"), $offering_locations);
            }

            $offerings[] = array(
                'name' => $offering->post_title,
                'id' => $offering->ID,
                'permalink' => get_permalink($offering->ID),
                'locations' => $locations,
            );
        }

        return $offerings;
    }

    public function mapLocations($offeringLocation)
    {
        return array(
            'id'     => $offeringLocation['location']->ID,
            'name'   => $offeringLocation['location']->post_title,
            'person' => $offeringLocation['cost_per_person']
        );
    }
}
