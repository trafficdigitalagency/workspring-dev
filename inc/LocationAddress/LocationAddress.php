<?php

namespace someoddpilot\LocationAddress;

class LocationAddress
{
    public $addressString = "";

    public function __construct($addressString)
    {
        $this->addressString = $addressString;

        $geocode = $this->getAddressGeocode();

        $this->setComponents($geocode->address_components);

        $this->lat = $geocode->geometry->location->lat;
        $this->lng = $geocode->geometry->location->lng;
    }

    private function setComponents($components)
    {
        if (empty($components)) {
            return;
        }

        foreach ($components as $component) {
            $type = $this->toCamelCase($component->types[0]);

            $this->$type = $component->short_name;
        }
    }

    private function toCamelCase($string)
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        $str[0] = strtolower($str[0]);

        return $str;
    }

    private function getAddressGeocode()
    {
        $query = http_build_query(
            array(
                'sensor' => 'false',
                'address' => $this->addressString,
            )
        );

        $jsonResponse = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?" . $query);

        $response = json_decode($jsonResponse);

        if ("OK" !== $response->status || empty($response->results[0]->address_components)) {
            return array();
        }

        return $response->results[0];
    }
}
