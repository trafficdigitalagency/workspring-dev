<?php

namespace someoddpilot\LocationAddress;

class LocationSearch
{
    public $locations = array();

    private $locationAddress;

    public function __construct($locationAddress)
    {
        $this->locationAddress = $locationAddress;

        $this->locations = $this->getLocations();

        usort($this->locations, function ($locationA, $locationB) {
            if ($locationA->distance == $locationB->distance) {
                return 0;
            }
            return ($locationA->distance < $locationB->distance) ? -1 : 1;
        });
    }

    private function getLocations()
    {
        return array_map(
            array($this, 'mapLocationDistances'),
            get_posts(
                array(
                    'post_type'      => 'location',
                    'posts_per_page' => -1
                )
            )
        );
    }

    private function mapLocationDistances($location)
    {
        $address = get_post_meta($location->ID, 'address', true);

        $location->lat      = floatval($address['lat']);
        $location->lng      = floatval($address['lng']);
        $location->address  = $address['address'];

        $distance = 9999999;

        if (!empty($location->lat) && !empty($location->lng)) {
            $distance = $this->distance(
                $this->locationAddress->lat,
                $this->locationAddress->lng,
                $location->lat,
                $location->lng
            );
        }

        $location->distance = $distance;

        return $location;
    }

    /**
     * Calculate greate-circle distance between two-points with the Haversine formula
     *
     * see http://stackoverflow.com/questions/14750275/haversine-formula-with-php
     *
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     *
     * @return float Distance between points in [m] (same as earthRadius)
     */
    private function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $sphereRadius = 3958)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo   = deg2rad($latitudeTo);
        $lonTo   = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 *
          asin(
              sqrt(
                  pow(sin($latDelta / 2), 2) +
                  cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)
              )
          );

        return $angle * $sphereRadius;
    }
}
