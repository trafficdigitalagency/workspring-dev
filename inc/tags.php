<?php
/**
 * Template tags
 */

function SOP_oddClass($index)
{
    if ($index % 2 === 0) {
        return "odd";
    }
}

function SOP_nthChildClass($index, $divisor = 3)
{
    if ($index % $divisor === 0) {
        return "n" . $divisor;
    }
}

function SOP_locationTabs()
{

    $tabs = array(
        array(
            "name"  => "general",
            "title" => __('General', 'workspring')
        )
    );

    if (get_field("offerings")) {
        while (has_sub_field("offerings")) {
            $tabs[] = array(
                "name"  => get_sub_field("type")->post_name,
                "title" => get_sub_field("type")->post_title,
            );
        } // endwhile offerings
    } // endif offerings

    if (get_field("categories") && !get_field("affiliate")) {
        $tabs[] = array(
            "title" => __("Local Guide", "workspring"),
            "name"  => "local-guide",
        );
    }

    $tabs[] = array(
        "title" => __("Find Us", "workspring"),
        "name"  => "find-us",
    );

    $buffer = "";

    foreach ($tabs as $tab) {
        $buffer .= "<li class=\"menu-item\" tab-trigger=\"" . $tab['name'] . "\">";
        $buffer .= "<a href=\"#!/" . $tab['name'] . "\">" . $tab['title'] . "</a></li>";
    }

    return $buffer;
}

function SOP_loop($callback)
{
    get_template_part("partials/head");

    if (have_posts()) {
        while (have_posts()) {
            the_post();

            $callback();
        }
    }

    get_template_part("partials/foot");

}

function SOP_loopOverLocations($callback)
{
    query_posts(
        array(
            "post_type"      => "location",
            "posts_per_page" => -1,
            "orderby"        => "title",
            "order"          => "ASC",
        )
    );
    if (have_posts()) {
        $callback();
    } // endif locations
    wp_reset_query();
}

function SOP_getLoopIndex()
{
    global $wp_query;

    return $wp_query->current_post;
}

function SOP_locationEventUrl()
{
    return (get_field("event_internal_link")) ? get_field("event_internal_link") : get_field("event_url");
}

function SOP_getRowIndex()
{
    $depth = count($GLOBALS['acf_field']) - 1;
    return $GLOBALS['acf_field'][ $depth ]['i'];
}

function SOP_getTemplateString($template)
{
    ob_start();
    get_template_part($template);
    return ob_get_clean();
}

function SOP_bootstrapOfferings()
{
    $offerings = get_posts(
        array(
            "post_type"      => "offering",
            "posts_per_page" => 4,
        )
    );

    if (!empty($offerings)) {
        return array(
            'WSPOfferingConfig' => array(
                'bookUrl'     => get_field("booking_page", "options"),
                'estimateUrl' => get_field("estimate_page", "options"),
            ),
            'WSPOfferingPricing' => SOP_getOfferingPricing($offerings)
        );
    }
}
