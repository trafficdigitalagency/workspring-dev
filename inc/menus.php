<?php
/**
 * General WP functions
 */

/**
 * get args for about nav
 *
 * @return array wp_nav_menu args
 */
function WSP_aboutNavArgs($args = array())
{
    return wp_parse_args(
        $args,
        array(
            'theme_location' => 'about',
            'container' => false,
            'menu_class' => 'header-about-menu',
            'depth' => 1,
            'walker' => new someoddpilot\Walker\AboutWalker(),
        )
    );
}

/**
 * register menus
 *
 * @return void
 */
function WSP_registerMenus()
{
    register_nav_menus(
        array(
            'primary'   => 'Primary',
            'secondary' => 'Secondary',
            'about'     => 'About',
            'footer'    => 'Footer',
        )
    );
}
