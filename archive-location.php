<?php

get_template_part("partials/head");

?>
<div class="container">
    <div class="about-listing-header">
        <h2>Locations</h2>
    </div>
    <?php
    global $wp_query;
    query_posts(
        wp_parse_args(
            array(
                "orderby" => "title",
                "order"   => "ASC",
            ),
            $wp_query->query_vars
        )
    );
    ?>
    <?php get_template_part('partials/listing'); ?>
</div>
<?php
get_template_part("partials/foot");
