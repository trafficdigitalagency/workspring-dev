<?php

get_template_part("partials/head");

if (have_posts()) {
    while (have_posts()) {
        the_post();

        ?>
        <div class="container">
            <div class="content-container">
                <h2><?php the_title(); ?></h2>

                <?php
                get_template_part("partials/post-header");
                get_template_part("partials/thumbnail");
                ?>

                <div class="wysiwyg">
                    <?php the_content(); ?>
                </div>

                <?php get_template_part("partials/post-footer"); ?>
            </div>

        </div>
        <?php
    } // endwhile posts
} // endif posts

get_template_part("partials/foot");
