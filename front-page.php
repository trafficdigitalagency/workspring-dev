<?php
get_template_part("partials/head");
?>
<h1 class="sr-only">Workspring | Team Spaces &amp; Coworking.</h1>
<div class="flexbox-column">
    <?php
    get_template_part("front-page/top-slider");
    get_template_part("front-page/under-slider-callout");
    ?>
    <div class="flex-2">
        <?php get_template_part("partials/offerings"); ?>
    </div>
    <?php
    get_template_part("front-page/location");
    get_template_part("front-page/news-slider");
    get_template_part("front-page/twitter");
    ?>
</div>
<?php
get_template_part("partials/foot");
