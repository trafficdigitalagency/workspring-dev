var gulp            = require('gulp'),
    jshint          = require('gulp-jshint'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    uglify          = require('gulp-uglify'),
    concat          = require('gulp-concat'),
    prefix          = require('gulp-autoprefixer'),
    kraken          = require('gulp-kraken'),
    gulpif          = require('gulp-if')

    //Variables
    input  = {
      'sassMaster'  : 'assets/sass/lander.sass',
      'sassWatch'   : 'assets/sass/**/*.sass'
    }

    output = {
      'css' : 'dist/css/'
    }
//Gulp Tasks
gulp.task('images', ['kraken'])
gulp.task('build', ['jshint', 'build-js','build-css','watch']);
gulp.task('default', ['watch'])


//Build CSS
gulp.task('build-css', function() {
  return gulp.src(input.sassMaster)
    .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(prefix("last 3 versions", "> 1%", "ie 8", "ie 7", "ie 6"))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output.css));
});

//Build JS
gulp.task('build-js', function(){
  gulp.src([
    'assets/js/custom.js'
  ])
  .pipe(concat('main.js'))
  .pipe(gulp.dest('dist/js'))
});

// JShint
gulp.task('jshint', function() {
  return gulp.src('assets/js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});


//Watch Tasks
gulp.task('watch', function() {
  gulp.watch('assets/js/**/*.js', ['jshint', 'build-js']);
  gulp.watch(input.sassWatch, ['build-css']);
  // watch nunjucks stuff
  gulp.watch([
      'templates/**/*',
    ]
  )
});
