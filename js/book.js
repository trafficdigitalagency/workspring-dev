define([
    'jquery',
    'underscore'
], function ($, _) {
    'use strict';

    var config = window.mainL10n.book,
        WSPOfferingPricing = window.WSPOfferingPricing;

    $('[book-calculator]').each(function (index, element) {
        require([
            'views/BookCalculatorView',
            'collections/PricingCollection'
        ], function (BookCalculatorView, PricingCollection) {
            function foreachOffering(offering) {
                var pricingCollection = new PricingCollection(offering.pricing);

                var locationPricing = pricingCollection
                    .getByLocationSlug(config.location);

                var price = $('<span class="book-price"></span'),
                    info = $('<span class="book-additional-outer"><span class="book-additional-info hidden"><span class="book-additional-inner">' + offering.info + '</span></span></span>');

                info.click(function () {
                    $(this).find('.book-additional-info').toggleClass('hidden');
                });

                $(element).append(price);

                if (offering.info && offering.info !== '') {
                    $(element).append(info)
                }

                new BookCalculatorView({
                    el:    price[0],
                    model: locationPricing
                });
            }


            _.each(
                _.where(WSPOfferingPricing, {
                    slug: config.offering
                }),
                foreachOffering
            );
        });
    });
});
