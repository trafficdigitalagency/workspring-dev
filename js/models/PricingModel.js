define([
    "backbone",
    "collections/PricingVariableCollection"
], function (Backbone, PricingVariableCollection) {
    "use strict";

    return Backbone.Model.extend({
        defaults: {
            location: {},

            variables: []
        },

        initialize: function () {
            this.set(
                "variables",
                new PricingVariableCollection(this.get("variables"))
            );

            this.set("locationID", this.get("location").ID);
        }
    });
});
