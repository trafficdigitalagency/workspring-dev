define([
    "backbone",
    "collections/PricingCollection"
], function (Backbone, PricingCollection) {
    "use strict";

    return Backbone.Model.extend({
        defaults: {
            id: 0,

            pricing: []
        },

        initialize: function () {
            this.set("pricing", new PricingCollection(this.get("pricing")));
        }
    });
});
