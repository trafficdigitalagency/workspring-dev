define([
    "backbone",
    "underscore"
], function (Backbone, _) {
    "use strict";

    return Backbone.Model.extend({
        defaults: {
            label:      "Click Here",
            url:        window.WSPOfferingConfig.bookUrl,
            parameters: []
        }
    });
});
