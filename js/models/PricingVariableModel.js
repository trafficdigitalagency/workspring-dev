define(["backbone"], function (Backbone) {
    "use strict";

    return Backbone.Model.extend({
        defaults: {
            label:      "",
            multiplier: 0,
            slug:       "",
            unit:       ""
        }
    });
});
