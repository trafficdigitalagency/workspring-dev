define(["marionette"], function (Marionette) {
    "use strict";

    var app = new Marionette.Application();

    return app;
});
