define([
    'backbone',
    'models/Post'
], function (backbone, Post) {
    'use strict';

    return backbone.Collection.extend({
        model: Post
    });
});
