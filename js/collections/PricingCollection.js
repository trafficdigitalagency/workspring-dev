define([
    "backbone",
    "models/PricingModel"
], function (Backbone, PricingModel) {
    "use strict";

    return Backbone.Collection.extend({
        model: PricingModel,

        getByLocationSlug: function (slug) {
            return this.filter(function (pricingLocation) {
                return (pricingLocation.get('location').post_name === slug);
            }).pop();
        }
    });
});
