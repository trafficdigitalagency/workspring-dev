define([
    "backbone",
    "models/PricingVariableModel"
], function (Backbone, PricingVariableModel) {
    "use strict";

    return Backbone.Collection.extend({
        model: PricingVariableModel
    });
});
