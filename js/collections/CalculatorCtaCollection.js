define([
    "backbone",
    "models/CalculatorCtaModel"
], function (Backbone, CalculatorCtaModel) {
    "use strict";

    return Backbone.Collection.extend({
        model: CalculatorCtaModel
    });
});
