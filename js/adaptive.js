define(["jquery"], function initialize($) {
    "use strict";

    var Adaptive = function (element, options) {
        this.element = element;

        this.options = $.extend({
            attribute: "adaptive-src",
            triggerWidth: 300
        }, options);

        this.addListener();
    };

    $.extend(Adaptive.prototype, {
        addListener: function () {
            var options = this.options,
                element = this.element;

            var resize = $.proxy(
                function testResize() {
                    if ($(window).width() < options.triggerWidth) {
                        return;
                    }

                    element.setAttribute(
                        "src",
                        element.getAttribute(options.attribute)
                    );

                    $(window).off("resize", resize);
                },
                this
            );

            $(window).on("resize", resize);

            resize();

            return this;
        }
    });

    return Adaptive;
});
