define(["jquery"], function ($) {
    "use strict";

    var Collapse = function (element) {
        this.element = $(element);

        this.trigger = $("[data-target=" + this.element.attr("id") + "]");

        this.init();
    };

    $.extend(Collapse.prototype, {
        init: function () {
            this.trigger.click($.proxy(this.toggle, this));
        },

        toggle: function () {
            this.element.toggleClass("in");
        }
    });

    return Collapse;
});
