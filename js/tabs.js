define(["jquery"], function ($) {
    "use strict";

    var triggerSelector = "tab-trigger",
        tabSelector = "tab",
        activeTabTrigger = "active",
        activeTab = "in";

    var Tabs = function(element) {
        this.element = $(element);

        this.init();
    };

    $.extend(Tabs.prototype, {
        init: function () {
            this.triggers = this.element.find("[" + triggerSelector + "]");

            this.tabs = this.element.find("[" + tabSelector + "]");

            this.attachEvents()
                .navigate();
        },

        attachEvents: function () {
            $(window).on("hashchange", $.proxy(this.navigate, this));

            return this;
        },

        navigate: function () {
            var name = window.location.hash
                // replace hashbang
                .replace(/^#!\//, "")
                // replace empty name with "general"
                .replace(/^$/, "general");

            var trigger = this.element.find(
                "[" + triggerSelector + "=" + name + "]"
            );

            var tab = this.element.find("[" + tabSelector + "=" + name + "]");

            this.triggers.each(function () {
                $(this).removeClass(activeTabTrigger);
            });

            this.tabs.each(function () {
                $(this).removeClass(activeTab);
            });

            trigger.addClass(activeTabTrigger);

            tab.addClass(activeTab);
        }
    });

    return Tabs;
});
