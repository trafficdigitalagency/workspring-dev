define(["jquery", "slick"], function ($) {
    "use strict";

    return function () {
        $("[slick=home]").slick({
            fade:          true,
            dots:          true,
            infinite:      true,
            autoplay:      true,
            autoplaySpeed: 8000,
            slide:         ".slide"
        });

        $("[slick=home-news]").slick({
            fade:           false,
            infinite:       true,
            slide:          ".slide",
            slidesToShow:   2,
            slidesToScroll: 2,
            responsive:     [
                {
                    breakpoint: 980,
                    settings: {
                        fade:           false,
                        dots:           true,
                        infinite:       true,
                        slide:          ".slide",
                        slidesToShow:   1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $("[slick=twitter]").slick({
            infinite:      true,
            speed:         500,
            autoplay:      true,
            autoplaySpeed: 8000,
            slide:         ".slide"
        });

        $("[slick=location-offering]").slick({
            infinite: true,
            slide:    ".slide"
        });

        $("[slick=content]").slick({
            infinite:      true,
            autoplay:      true,
            autoplaySpeed: 8000,
            slide:         ".slide"
        });
    };
});
