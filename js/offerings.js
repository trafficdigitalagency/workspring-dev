define(["jquery"], function ($) {
    "use strict";

    $(".offering-overview").click(function () {
        var thisOffering = $(this).closest(".offering");

        $(this).closest(".offerings")
                .toggleClass("has-active-offering")
            .children(".offering").not(thisOffering)
                .removeClass("active-offering");
        thisOffering
            .toggleClass("active-offering");

        window.ga("send", "event", "calculator", "click", "calculator", $(this).find("h2").text());
    });

    $(".close-offering").click(function () {
        $(this).closest(".offering").removeClass("active-offering");
        $(this).closest(".offerings").removeClass("has-active-offering");
    });

    require([
        "views/CalculatorView",
        "models/OfferingModel",
        "collections/PricingCollection",
        "underscore"
    ], function (CalculatorView, OfferingModel, PricingCollection, _) {
        $(".offering-pricing-calculator").each(function () {

            var offering = _.findWhere(window.WSPOfferingPricing, {
                id: $(this).data("id")
            });

            var calculator = new CalculatorView({
                el:         this,
                offering:   offering,
                offeringID: offering.id,
                collection: new PricingCollection(offering.pricing)
            });

            calculator.render();

            $(this).data(
                "calculator",
                calculator
            );
        });
    });
});
