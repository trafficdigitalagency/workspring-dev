define(function () {
    "use strict";

    return function (href) {
        var callback = function () {
            var link = document.createElement("link");
            link.rel = "stylesheet";
            link.href = href;
            var head = document.getElementsByTagName("head")[0];
            head.parentNode.insertBefore(link, head);
        };

        var requestAnimationFrame = window.requestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.msRequestAnimationFrame;

        if (requestAnimationFrame) {
            requestAnimationFrame(callback);
        } else {
            window.addEventListener("load", callback);
        }
    };
});
