requirejs.config({
    baseUrl: "/workspring-dev/wp-content/themes/workspring/js",
    paths: {
        jquery:       "shims/jquery",
        fitvids:      "//cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.min",
        backbone:     "//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min",
        marionette:   "//cdnjs.cloudflare.com/ajax/libs/backbone.marionette/1.8.6/backbone.marionette.min",
        underscore:   "//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min",
        slick:        "../bower_components/slick-carousel/slick/slick",
        dropdown:     "../bower_components/bootstrap/js/dropdown",
        tooltip:      "../bower_components/bootstrap/js/tooltip",
        popover:      "../bower_components/bootstrap/js/popover",
        collapse:     "../bower_components/bootstrap/js/collapse",
        transition:   "../bower_components/bootstrap/js/transition",
        placeholders: "vendor/gf.placeholders"
    },
    shim: {
        fitvids: {
            deps: ["jquery"]
        },
        marionette: {
            exports: "Backbone.Marionette",
            deps: ["backbone"]
        },
        slick: {
            deps: ["jquery"]
        },
        tooltip: {
            deps: ["jquery"]
        },
        popover: {
            deps: ["tooltip", "jquery"]
        },
        transition: {
            deps: ["jquery"]
        },
        dropdown: {
            deps: ["jquery"]
        },
        collapse: {
            deps: ["transition", "jquery"]
        },
        placeholders: {
            deps: ["jquery"]
        }
    }
});

require(["underscore", "jquery", "vendor/modernizr.min"], function (_, $) {
    "use strict";

    function inIframe () {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }

    if (inIframe()) {
        $("html").addClass("iframe");
    }

    require(["adaptive"], function (Adaptive) {
        $("[adaptive-src]").each(function () {
            new Adaptive(this);
        });
    });

    require(["tabs"], function (Tabs) {
        $("[tabs]").each(function () {
            new Tabs(this);
        });
    });

    require(["modal"], function (Modal) {
        $("[modal]").each(function () {
            new Modal(this);
        });
    });

    require(["sliders"], function (sliders) {
        sliders();
    });

    require(["dropdown"]);

    require(["offerings"]);

    require(["popover"], function () {
        $("[data-toggle=popover]").popover();
    });

    require(["collapse"], function () {
        $(".faq-list dt").addClass("collapsed");
        $(".faq-list dd").addClass("collapse");
    });

    if (!window.location.href.match(/iframe=1/)) {
        require(["olark"]);
    }

    $(".gfield_required").remove();

    $(".gfhidelabel").each(function () {
        $(this).find(".gfield_label").hide();
    });

    $(".header-dropdown-menu .menu-item").find('a').click(function () {
        $('.header-dropdown-menu .btn').text($(this).closest('.menu-item').text());
    });

    require(["forms"]);

    require(["placeholders"]);

    require(["book"]);

    require(["waterfall"], function (Waterfall) {
        $("body.blog [sop-waterfall]").each(function (index, element) {
            $(element).data("waterfall", new Waterfall(element));
        });
    });

    if ("ontouchstart" in window) {
        var clickSubmenuTrigger = function (event) {
            event.preventDefault();

            $(this).closest('.menu-item').addClass('active');
            $(this).off("click", clickSubmenuTrigger);
        };
        $(".primary-menu .sub-menu").siblings("a").click(clickSubmenuTrigger);
    }

    if ($(".wysiwyg iframe").length > 0) {
        require(["fitvids"], function () {
            $(".wysiwyg").fitVids();
        });
    }
});
