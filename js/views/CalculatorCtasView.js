define([
    "marionette",
    "views/CalculatorCtaView"
], function (Marionette, CalculatorCtaView) {
    "use strict";

    return Marionette.CollectionView.extend({
        // Item view
        itemView: CalculatorCtaView,

        initialize: function () {
            var parent = this;

            this.on("values:changed", this.valuesChanged);
        },

        valuesChanged: function (values) {
            this.children.each(function (child) {
                child.valuesChanged(values);
            });
        }
    });
});
