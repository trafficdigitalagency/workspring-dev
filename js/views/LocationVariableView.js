define([
    "marionette",
    "underscore",
    "templates"
], function (Marionette, _, templates) {
    "use strict";

    return Marionette.ItemView.extend({
        template: templates.locationVariable,

        ui: {
            "selected": ".dropdown-selected"
        },

        events: {
            "click .dropdown-item": "select"
        },

        attributes: function () {
            return {
                "class":        "dropdown",
                "data-selects": this.model.get("slug")
            };
        },

        select: function (event) {
            this.ui.selected.text(Marionette.$(event.currentTarget).text());

            var selectedOption = _.findWhere(
                this.model.get("options"), {
                    // append string to force stringyness
                    slug: Marionette.$(event.currentTarget).data("value") + ""
                }
            );

            this.trigger(
                "variable:option:selected",
                this.model.get("slug"),
                selectedOption
            );
        }
    });
});
