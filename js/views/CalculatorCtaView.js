define([
    "marionette",
    "templates",
    "apps/CalculatorApp",
    "underscore",
    "modal"
], function (Marionette, templates, CalculatorApp, _, Modal) {
    "use strict";

    return Marionette.ItemView.extend({
        template: function (object) {
            return object.label;
        },

        tagName: "a",

        attributes: function () {
            return {
                href:           this.model.get("url"),
                "class":        "btn btn-primary calculator-action show",
                modal:          "",
                "modal-iframe": ""
            };
        },

        onRender: function () {
            new Modal(this.$el);
        },

        initialize: function () {
            this.valuesChanged({});
        },

        valuesChanged: function (values) {
            var modelParams = _.object(_.map(
                this.model.get("parameters"),
                function (param) {
                    return [
                        param.key.replace("book-", ""),
                        {
                            slug: param.value
                        }
                    ];
                }
            ));

            values = _.defaults(modelParams, values);

            var params = _.reduce(values, function (memo, value, key) {
                if (value === null) {
                    return memo;
                }
                return memo + "&book-" + key + "=" + value.slug;
            }, "calculator=1");

            this.el.search = "?v=1&";

            if (values.offering && values.location) {
                params = params + "&form-page=2";
            }

            this.$el.attr("href", this.$el.attr("href").replace(/$/, params));
        }
    });
});
