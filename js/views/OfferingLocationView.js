define([
    "marionette",
    "templates"
], function (Marionette, templates) {
    "use strict";

    return Marionette.ItemView.extend({
        template: templates.offeringLocation,

        tagName: "li",

        attributes: function () {
            return {
                "class":      "dropdown-item",
                "data-value": this.model.get("location").ID
            };
        }
    });
});
