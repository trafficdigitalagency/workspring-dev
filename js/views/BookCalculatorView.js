define([
    'backbone',
    'jquery',
    'underscore'
], function (Backbone, $, _) {
    'use strict';

    return Backbone.View.extend({
        values: {},

        initialize: function () {
            this.model.get('variables').each($.proxy(function (model) {
                $('.book-' + model.get('slug') + ' select').each(
                    $.proxy(
                        function (index, element) {
                            var callback = $.proxy(function () {
                                this.setValue(model, $(element).val());
                            }, this);

                            callback();

                            $(element).change(callback);
                        },
                        this
                    )
                );
            }, this));
        },

        setValue: function (model, value) {
            var optionValue = _.findWhere(model.get('options'), {
                slug: value
            });

            var defaultValue = {
                unit: '',
                multiplier: 0
            };

            this.values[model.get('slug')] = (optionValue) ?
                optionValue : defaultValue;

            this.calculate();
        },

        getPrice: function () {
            return _.reduce(this.values, function (memo, value) {
                var multiplier = (value && value.multiplier) ?
                    value.multiplier : 0;
                return memo * parseInt(multiplier, 10);
            }, 1);
        },

        getPrettyPrice: function (price) {
            return _.map(_.filter(this.values, function (value) {
                return (parseInt(value.multiplier, 10) !== 1 && value.unit !== '');
            }), function (value) {
                return '&times; $' + value.multiplier + ' ' + value.unit;
            }).join('<br>') + ' = $' + price;
        },

        calculate: function () {
            var price = this.getPrice(),
                prettyPrice = 'We&rsquo;ll contact you to discuss options tailored to your team&rsquo;s unique needs.';

            if (price !== 0) {
                prettyPrice = this.getPrettyPrice(price);
            }

            this.$el.html(prettyPrice);
        }
    });
});
