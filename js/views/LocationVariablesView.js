define([
    "marionette",
    "views/LocationVariableView",
    "templates"
], function (Marionette, LocationVariableView, templates) {
    "use strict";

    return Marionette.CompositeView.extend({
        // Item view
        itemView: LocationVariableView,

        itemViewContainer: ".variables-dropdowns",

        template: templates.locationVariables,

        itemViewOptions: function () {
            return {
                type: this.type
            };
        },

        initialize: function (options) {
            this.type = options.type;
        }
    });
});
