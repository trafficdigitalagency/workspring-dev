define([
    'marionette',
    'collections/PostCollection'
], function (Marionette, PostCollection) {
    'use strict';

    return Marionette.CollectionView.extend({
        collection: new PostCollection(),

        initialize: function () {
            Marionette.$.get(mainL10n.apiUrl + '/get_recent_posts', function () {
            });
        }
    });
});
