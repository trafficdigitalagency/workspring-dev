(function (WSPOfferingConfig) {
    "use strict";

    define([
        "marionette",
        "views/OfferingLocationView",
        "apps/CalculatorApp",
        "underscore",
        "templates"
    ], function (Marionette, OfferingLocationView, calculatorApp, _, templates) {
        return Marionette.CompositeView.extend({
            // Item view
            itemView: OfferingLocationView,

            itemViewContainer: "[data-selects=location] .dropdown-menu",

            ui: {
                price:     ".price-amount",
                variables: ".variables",
                email:     "[data-action=email]",
                book:      "[data-action=book]",
                location:  "[data-selects=location] .dropdown-selected"
            },

            events: {
                "click [data-selects=location] .dropdown-item": "clickLocation"
            },

            location: null,

            values: {},

            template: templates.calculator,

            initialize: function () {
                this.on("render:variables", this.renderVariables);

                _.bindAll(this, "initCtas", "initVariables");

                require(
                    [
                        "views/CalculatorCtasView",
                        "collections/CalculatorCtaCollection"
                    ],
                    this.initCtas
                );
            },

            initCtas: function (CalculatorCtasView, CalculatorCtaCollection) {
                this.ctasView = new CalculatorCtasView({
                    el:         this.$(".ctas"),
                    collection: new CalculatorCtaCollection(
                        this.options.offering.ctas
                    )
                }).render().listenTo(
                    this,
                    "values:changed",
                    function (values) {
                        this.trigger("values:changed", values);
                    },
                    this.ctasView
                );
            },

            initVariables: function (LocationVariablesView) {
                this.ui.location.text(
                    this.location.get("location").post_title
                );

                var variables = this.location.get("variables");

                this.variablesView = new LocationVariablesView({
                    el:         this.ui.variables,
                    collection: variables,
                    app:        this.app
                });

                this.variablesView.on(
                    "itemview:variable:option:selected",
                    this.onVariableChange,
                    this
                );

                this.variablesView.render();

                var values = {};

                variables.each(
                    function (variable) {
                        values[variable.get("slug")] = null;
                    }
                );

                this.values = values;

                this.renderPrice();
            },

            onRender: function () {
                var modals = this.$("[modal]");

                require(["modal"], function (Modal) {
                    modals.each(function () {
                        new Modal(this);
                    });
                });
            },

            clickLocation: function (event) {
                var locationID = Marionette.$(event.currentTarget)
                    .data("value");

                this.location = this.collection.findWhere({
                    locationID: locationID
                });

                this.triggerValuesChanged();

                require(["views/LocationVariablesView"], this.initVariables);
            },

            onVariableChange: function (view, type, value) {
                this.values[type] = value;

                this.renderPrice();
            },

            getLocationSlug: function () {
                return this.location.get("location").post_name;
            },

            triggerValuesChanged: function () {
                var values = _.defaults(
                    this.values,
                    {
                        offering: {
                            multiplier: 1,
                            label: "",
                            unit: "",
                            slug: this.options.offering.slug
                        },
                        location: {
                            multiplier: 1,
                            label: "",
                            unit: "",
                            slug: this.getLocationSlug()
                        }
                    }
                );

                this.trigger(
                    "values:changed",
                    values
                );
            },

            renderPrice: function () {
                this.ui.price.text(
                    this.getPrice()
                );

                this.triggerValuesChanged();

                if (this.isIncompleteSelection()){
                    this.ui.email.removeClass("show");

                    this.ui.book
                        .attr(
                            "href",
                            WSPOfferingConfig.bookUrl
                        );
                } else {
                    this.ui.book
                        .attr(
                            "href",
                            WSPOfferingConfig.bookUrl + this.getBookParams()
                        );

                    this.ui.email.addClass("show")
                        .attr(
                            "href",
                            WSPOfferingConfig.estimateUrl + "?estimate-message=" +
                                encodeURIComponent(
                                    this.options.offering.estimateText + " " +
                                        this.getPrice()
                                )
                        );
                }
            },

            getBookParams: function () {
                return _.reduce(
                    this.values,
                    function (memo, value, type) {
                        return memo + "&book-" + type + "=" + value.slug;
                    },
                    "?form-page=2&book-location=" + this.getLocationSlug()
                );
            },

            isIncompleteSelection: function () {
                return _.contains(this.values, null);
            },

            getPrice: function () {
                if (this.isIncompleteSelection()) {
                    return "";
                }

                var price = _.reduce(
                    this.values,
                    function (memo, value) {
                        if (!value || !value.multiplier) {
                            return 0;
                        }

                        return memo * value.multiplier;
                    },
                    1
                );

                var unit = _.reduce(
                    this.values,
                    function (memo, value) {
                        return memo + value.unit;
                    },
                    " "
                );

                if (price === 0) {
                    return "We’ll contact you to discuss options tailored to your team’s unique needs.";
                }

                return "Price Estimate: $" + price + unit;
            }
        });
    });
}(window.WSPOfferingConfig));
