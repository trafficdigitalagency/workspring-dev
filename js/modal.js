define([
    "jquery",
    "underscore",
    "templates"
], function ($, _, templates) {
    "use strict";

    var Modal = function(element, options) {
        this.element = $(element);

        this.options = $.extend({
            iframe: false
        }, options);

        this.init();

        this.body = $("body");
    };

    $.extend(Modal.prototype, {
        init: function () {
            _.bindAll(
                this,
                "open",
                "close",
                "escapePressed"
            );

            this.element.click(this.open);

            $(window).on("hashchange", this.close);
        },

        open: function (event) {
            if (window.Modernizr.touch) {
                return;
            }

            event.preventDefault();

            this.insertModal()
                .insertBackground();

            setTimeout(_.bind(function () {
                this.background.addClass("in");

                this.modal.addClass("in");
            }, this), 25);
        },

        getIframeSrc: function () {
            var element = this.element,
                src = element.attr("href");

            if (element.attr("modal-video") !== undefined) {
                src = src
                    .replace(/watch\?v=/, "embed/")
                    .replace(/$/, "?autoplay=1&modestbranding=1");
            }

            return src;
        },

        insertModal: function () {
            var element = this.element,
                src = this.getIframeSrc(),
                contentTemplate = templates.iframe;

            if (element.attr("modal-video") !== undefined) {
                contentTemplate = templates.iframeVideo;
            }

            var modal = $(templates.modal({
                content: contentTemplate({
                    src: src
                })
            }));

            $("#page").append(modal);

            this.modal = modal;

            this.attachEvents();

            return this;
        },

        attachEvents: function () {
            this.modal.click(this.close);

            $(document).keyup(this.escapePressed);
        },

        escapePressed: function (event) {
            if (event.keyCode === 27) {
                this.close();
            }
        },

        removeEvents: function () {
            var close = this.close;

            this.modal.off("click", close);

            $(document).off("keyup", function (event) {
                if (event.keyCode === 27) {
                    close();
                }
            });
        },

        insertBackground: function () {
            var background = $("<div></div>"),
                body = this.body;

            this.background = background;

            background
                .addClass("modal-backdrop")
                .addClass("fade");

            body.append(background);

            body.addClass("modal-open");

            return this;
        },

        close: function () {
            var modal = this.modal,
                background = this.background;

            if (modal) {
                modal
                    .off("click", this.close)
                    .removeClass("in");

            }
            if (background) {
                background.removeClass("in");
            }

            this.body.removeClass("modal-open");

            setTimeout(function () {
                if (modal) {
                    modal.remove();
                }

                if (background) {
                    background.remove();
                }
            }, 500);
        }
    });

    return Modal;
});
