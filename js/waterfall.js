(function (waterfall) {
    'use strict';

    define(['jquery'], function ($) {

        var Waterfall = function (element) {
            this.el   = element;
            this.$el  = $(element);
            this.$lm  = $('<button class="btn btn-primary center-block">LOAD MORE</button>');
            this.$li  = this.$el.find('.about-listing');

            this.init();
        };

        $.extend(Waterfall.prototype, {
            page: 1,

            busy: false,

            done: false,

            init: function () {
                this.$el
                    .find('.pagination').remove().end()
                    .append(
                        $('<div class="load-more-outer" style="padding-bottom: 40px;"></div>').append(this.$lm)
                    );

                this.$lm.click($.proxy(this.click, this));

                $(window).scroll($.proxy(this.scroll, this));
            },

            click: function () {
                if (this.busy) {
                    return;
                }

                this.busy = true;
                $.get(waterfall.apiUrl, {
                    action:  'waterfall',
                    from:    window.location.href,
                    page: this.page
                }, $.proxy(this.add, this));
            },

            scroll: function () {
                var windowBottom = ($(window).height() + $(window).scrollTop());

                if (this.$lm.offset().top < windowBottom) {
                    this.click();
                }
            },

            add: function (html, status) {
                this.busy = false;
                if (status !== 'success') {
                    return;
                }
                if (html === '') {
                    this.$lm.fadeOut(250);
                    this.done = true;
                }
                this.page = this.page + 1;

                var $html = $(html);

                require(['modal'], function (Modal) {
                    $html.find('[modal]').each(function () {
                        new Modal(this);
                    });
                });

                this.$li.append($html)
                    .children(':nth-child(3n+1)').addClass('n3');
            }
        });

        return Waterfall;
    });
}(window.waterfall));
