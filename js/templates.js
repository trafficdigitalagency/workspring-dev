define(function(){

this["JST"] = this["JST"] || {};

this["JST"]["calculator"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="dropdowns">\n    <div class="dropdown" data-selects="location">\n        <a href="javascript:void(0)" class="dropdown-selected btn btn-default" data-toggle="dropdown">\n            Location\n        </a>\n        <ul class="dropdown-menu"></ul>\n    </div>\n\n    <div class="variables"></div>\n</div>\n\n<div class="price-amount"></div>\n\n<a href="javascript:void(0)" class="calculator-action"\n    data-action="email" modal modal-iframe>\n    Email Estimate\n</a>\n\n<div class="ctas"></div>\n';

}
return __p
};

this["JST"]["iframe"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="embed-responsive embed-responsive-4by3">\n    <iframe width="320" height="240"\n        src="' +
((__t = ( src )) == null ? '' : __t) +
'"></iframe>\n</div>\n';

}
return __p
};

this["JST"]["iframeVideo"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="embed-responsive embed-responsive-16by9">\n    <iframe width="320" height="240" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"\n        src="' +
((__t = ( src )) == null ? '' : __t) +
'"></iframe>\n</div>\n';

}
return __p
};

this["JST"]["locationVariable"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<a href="javascript:void(0)" class="dropdown-selected btn btn-default" data-toggle="dropdown">\n    ' +
((__t = ( type )) == null ? '' : __t) +
'\n</a>\n<ul class="dropdown-menu">\n    ';
 _.each(options, function (option) { ;
__p += '\n    <li class="dropdown-item" data-value="' +
((__t = ( option.slug )) == null ? '' : __t) +
'">\n        <a href="javascript:void(0);">' +
((__t = ( option.label )) == null ? '' : __t) +
'</a>\n    </li>\n    ';
 }); ;
__p += '\n</ul>\n';

}
return __p
};

this["JST"]["locationVariables"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="variables-dropdowns"></div>\n';

}
return __p
};

this["JST"]["modal"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class=\'modal fade bs-example-modal-lg\' tabindex=\'-1\' role=\'dialog\' aria-labelledby=\'myLargeModalLabel\' aria-hidden=\'true\'>\n    <div class=\'modal-dialog modal-lg\'>\n        <div class=\'modal-content\'>\n            <div class="close-modal">&times;</div>\n            ' +
((__t = ( content )) == null ? '' : __t) +
'\n        </div>\n    </div>\n</div>\n';

}
return __p
};

this["JST"]["offeringLocation"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<a href="javascript:void(0)">\n    ' +
((__t = ( location.post_title )) == null ? '' : __t) +
'\n</a>\n';

}
return __p
};

  return this["JST"];

});