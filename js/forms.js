define(["jquery"], function ($) {
    "use strict";

    $(".gfield_label").filter(function () {
        return ($(this).text() === "*");
    }).remove();

    $("select").wrap("<div class='select-wrapper'></div>");

    if ($(".datepicker").length > 0) {
        require(["style-loader"], function (styleLoader) {
            styleLoader("//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css");
        });
    }

    $(".gfield_contains_required").find(":text, [type=email], [type=tel], select").filter(":visible").attr("required", "required");
});
