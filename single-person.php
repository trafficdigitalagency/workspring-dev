<?php

get_template_part("partials/head");

if (have_posts()) {
    while (have_posts()) {
        the_post();
        get_template_part('partials/header', 'about');
        ?>
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="person-title">
                        <h2><?php the_title(); ?></h2>

                        <h3 class="large-heading gray"><?php the_field("position"); ?></h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <h3 class="sr-only">About <?php the_title(); ?></h3>
                <div class="col-md-4 col-sm-6 col-md-offset-2">
                    <?php get_template_part("partials/thumbnail"); ?>
                </div>

                <div class="col-md-4 col-sm-6 wysiwyg">
                    <?php the_content(); ?>
                </div>
            </div>

            <?php
            query_posts(
                array(
                    "ignore_sticky_posts" => true,
                    "post_type"           => "post",
                    "meta_key"            => "author",
                    "meta_value"          => get_the_id(),
                )
            );

            if (have_posts()) {
                ?>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="large-heading">Related Articles</h3>

                        <div class="row">
                        <?php
                            while (have_posts()) {
                                the_post();

                                ?>
                                <div class="col-md-6">
                                    <?php get_template_part("partials/listing-item"); ?>
                                </div>
                                <?php
                            } // endwhile posts
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            } // endif posts

            wp_reset_query(); ?>

            <div class="row" style="padding: 20px 0;">
                <div class="col-md-8 col-md-offset-2">
                    <div class="post-footer">
                        <a href="<?php echo get_post_type_archive_link("person"); ?>" class="btn btn-primary">BACK</a>
                    </div>
                </div>
            </div>

        </div>
        <?php
    }
}

get_template_part("partials/foot");
